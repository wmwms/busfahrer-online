import { Game } from '../game';
import { Server } from 'socket.io';
import { SkatPlayer } from './SkatPlayer';
import * as helper from '../../util/helperFunctions';
import { isBoolean } from 'util';

export class SkatGame implements Game {
    id: string;
    io: Server;
    playerMap: Map<SkatPlayer, string[]> = new Map();
    phase = 0; // 0readyPhase, 1skatTravel, 2putAwaySkat, 3selectGame, 4game, 5result
    maxPlayerCount: number = 3;
    hasStarted = false;

    firstPlayer: SkatPlayer;
    secondPlayer: SkatPlayer;
    thirdPlayer: SkatPlayer;

    currentPlayer: SkatPlayer;
    playingPlayer: SkatPlayer;
    playingPlayerStack: string[] = [];

    // skatTravel
    currentSkattravelIndex: number = 0; // 0 => 18 in array
    skatTravelArray: number[] = this.createSkatTravelArray();

    // game
    currentGameType: number;
    thirdCardTimeOut = false;

    isRamsch = false;
    isGrand = false;

    constructor(id, io) {
        this.id = id;
        this.io = io;
    }

    async playerJoin(socketId) {
        let player = new SkatPlayer(socketId);
        if (!this.hasStarted) {
            if (this.getPlayerCount() < this.maxPlayerCount && await player.getUserWithSocketId()) {
                if (player.user.username == null || /^\s*$/.test(player.user.username)) {
                    this.io.to(socketId).emit('msg', 'You will have to set a valid Username before you can ready up!');
                    this.io.to(this.id).emit('msg', "New Player joined!");
                }
                else {
                    this.io.to(this.id).emit('msg', player.user.username + " joined!");
                }
                this.playerMap.set(player, []);
                //this.io.to(socketId).emit("join");
            }
        }
        else {
            this.io.to(socketId).emit('msg', "Spiel hat schon begonnen");
            this.sendGameStateToSocket(socketId);
        }
    }

    // rm player from playermap
    playerLeave(socketId) {
        let player = this.getPlayerWithSocketId(socketId);
        // TODO put cards back in deck;
        if (player) {
            this.sendGameStateToRoom();
            return this.playerMap.delete(player);
        }
        return false;
    }

    // set player as ready
    async playerReady(socketId) {
        if (!this.hasStarted) {
            let player = this.getPlayerWithSocketId(socketId);
            if (player && await player.getUserWithSocketId()) {
                if (player.user.username == null || /^\s*$/.test(player.user.username)) {
                    this.io.to(socketId).emit('msg', 'You will have to set a valid Username before playing!');
                }
                else {
                    player.ready = true;
                    this.io.to(this.id).emit('msg', player.user.username + " ist bereit!" + this.getPlayerReadyCount() + "/" + this.getPlayerCount());
                    if (this.maxPlayerCount == this.getPlayerReadyCount()) {
                        this.start();
                    }
                }

            }

        }
    }

    getPlayerCount(): number {
        let count = 0;
        for (let player of this.playerMap.keys()) {
            if (player.socketID != "skat" && player.socketID != "table") {
                count++;
            }
        }
        return count;
    }

    async getClientSocketCount(): Promise<number> {
        return await (await this.io.in(this.id).allSockets()).size;
    }

    getPlayerReadyCount(): number {
        let count = 0;
        for (let player of this.playerMap.keys()) {
            if (player.socketID != "skat" && player.socketID != "table" && player.ready) {
                count++;
            }
        }
        return count;
    }

    getPlayerWithSocketId(socketID) {
        for (let player of this.playerMap.keys()) {
            if (player.socketID == socketID) {
                return player;
            }
        }
        return null;
    }
    start() {
        this.io.to(this.id).emit('msg', 'Spiel beginnt');
        this.hasStarted = true;
        this.phase = 1;
        this.playingPlayerStack = [];
        this.currentSkattravelIndex = 0;
        for (let player of this.playerMap.keys()) {
            player.passedSkatTravel = false;
        }
        this.createNewPlayerMap();
        this.skatTravelBegin();
        this.sendGameStateToRoom();
    }

    sendGameStateToSocket(socketID) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && this.hasStarted) {
            this.io.to(socketID).emit('selfUpdate', this.getPublicPlayerCards(socketID), player);
            this.io.to(socketID).emit('ownCards', this.getPublicPlayerCards(socketID));
            this.io.to(socketID).emit('playerMap', JSON.stringify([...this.getPublicPlayerMap(socketID)]));
            this.io.to(this.id).emit('phase', this.phase);
        }
    }

    sendGameStateToRoom() {
        if (this.hasStarted) {
            //this.io.to(this.id).emit('playerMap', JSON.stringify([...this.getPublicPlayerMap()]));
            for (let player of this.playerMap.keys()) {
                if (player.socketID != 'skat' && player.socketID != 'table') {
                    this.io.to(player.socketID).emit('selfUpdate', this.getPublicPlayerCards(player.socketID), player);
                    this.io.to(player.socketID).emit('playerMap', JSON.stringify([...this.getPublicPlayerMap(player.socketID)]));
                }
            }
            this.io.to(this.id).emit('phase', this.phase);
            this.io.to(this.id).emit('tableCards', this.playerMap.get(this.getPlayerWithSocketId('table')));
        }
    }

    createNewPlayerMap() {
        let cards = helper.create7ADeckGerman();
        // add all cards to deck

        this.playerMap.set(new SkatPlayer("table"), []);
        let skatCards = [];
        //set 2 cards to skat
        for (let i = 0; i < 2; i++) {
            skatCards.push(cards[i]);
        }
        this.playerMap.set(new SkatPlayer("skat"), skatCards);

        // set 10 cards to every player
        let i = 2;
        this.playerMap.forEach((value: string[], key: SkatPlayer) => {
            // only set in very first round -> later first becomes second, second becomes third, third becomes first
            if (this.firstPlayer == null) {
                this.firstPlayer = key;
            }
            else if (this.secondPlayer == null) {
                this.secondPlayer = key;
            }
            else if (this.thirdPlayer == null) {
                this.thirdPlayer = key;
            }

            if (key.socketID != "skat" && key.socketID != "table") {
                this.playerMap.set(key, [cards[i], cards[i + 1], cards[i + 2], cards[i + 3], cards[i + 4], cards[i + 5], cards[i + 6], cards[i + 7], cards[i + 8], cards[i + 9]]);
                key.maxSkatTravel = this.getPlayerMaxSkatTravel(key);
                i = i + 10;
            }

        });
        return this.playerMap;
    }

    getPublicPlayerCards(socketID) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player) {
            return this.playerMap.get(player);
        }
        return null;
    }

    getPublicPlayerMap(socketID) {
        let returnMap: Map<Object, string[]> = new Map<Object, string[]>();
        this.playerMap.forEach((value: string[], key: SkatPlayer) => {
            if (key.socketID != socketID && key.socketID != 'skat' && key.socketID != 'table') {
                let valueArr = JSON.parse(JSON.stringify(value)); // deep copy so original does not get changed
                for (let cardVal of valueArr) {
                    valueArr[valueArr.indexOf(cardVal)] = "card_back";
                }
                if (key.user && key.user.username) {
                    returnMap.set({ username: key.user.username, gamePoints: key.gamePoints }, valueArr);
                }
            }
        });

        return returnMap;
    }

    // Phase 1 skat travel

    getConnectingBubeCount(cards: string[]): number {
        let count = 2;
        let withJC = false;
        if (cards.includes('UE')) {
            withJC = true;
        }
        let restOfBubes = ['UG', 'UH', 'US'];
        for (let bube of restOfBubes) {
            if (cards.includes(bube) && withJC) {
                count++;
            }
            else if (!cards.includes(bube) && !withJC) {
                count++;
            }
            else {
                return count;
            }
        }
        return count;
    }

    createSkatTravelArray() {
        let values = [9, 10, 11, 12, 23, 24];
        let arr: number[] = [];
        for (let val of values) {
            for (let i = 2; i < 8; i++) {
                arr.push(val * i);
            }
        }
        return arr.sort((a, b) => a - b);
    }

    getPlayerMaxSkatTravel(player: SkatPlayer): number {
        let cards = this.playerMap.get(player);
        let multVal = this.getConnectingBubeCount(cards) + player.playingHand + player.playingOuvert;
        return player.playingCardType * multVal;
    }

    playerSetPlayingTypeValue(socketID, value: number) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && this.phase < 4) {
            player.playingCardType = value;
            player.maxSkatTravel = this.getPlayerMaxSkatTravel(player);
            this.sendGameStateToSocket(socketID);
        }
    }

    playerToggleOuvert(socketID) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && this.phase < 4) {
            player.toggelPlayingOuvert()
            player.maxSkatTravel = this.getPlayerMaxSkatTravel(player);
            this.sendGameStateToSocket(socketID);

        }
    }

    playerToggleHand(socketID) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && this.phase < 4) {
            player.togglePlayingHand();
            player.maxSkatTravel = this.getPlayerMaxSkatTravel(player);
            this.sendGameStateToSocket(socketID);

        }
    }

    skatTravelBegin() {
        this.currentPlayer = this.secondPlayer; // reizen beginnt bei 2. spieler
        this.io.to(this.currentPlayer.socketID).emit('skatTravelValue', this.skatTravelArray[this.currentSkattravelIndex]);
    }

    skatTravelSetNextPlayer() {
        let passedCount = 0;
        let out: SkatPlayer;
        let notOut: SkatPlayer;
        for (let player of this.playerMap.keys()) {
            if (player.socketID != 'skat' && player.socketID != 'table') {
                if (player.passedSkatTravel) {
                    passedCount++;
                    // use just in case there is only one out
                    out = player;
                }
                else {
                    // use just in case there is only one not out
                    notOut = player;
                }
            }

        }
        if (passedCount == 0) {
            if (this.currentPlayer == this.firstPlayer) {
                // secondPlayer  has to call next number or pass

                this.currentSkattravelIndex++;
                this.currentPlayer = this.secondPlayer;
            }
            else if (this.currentPlayer == this.secondPlayer) {
                // first player has to match or pass
                this.currentPlayer = this.firstPlayer;
            }
        }
        if (passedCount == 1) {
            if (out == this.firstPlayer) {
                if (this.currentPlayer == this.firstPlayer) {
                    // thid player has to call next number or pass
                    this.currentSkattravelIndex++;
                    this.currentPlayer = this.thirdPlayer;
                }
                else if (this.currentPlayer == this.secondPlayer) {
                    // thid player has to call next number or pass
                    this.currentSkattravelIndex++;
                    this.currentPlayer = this.thirdPlayer;
                }
                else if (this.currentPlayer == this.thirdPlayer) {
                    // second player has to match or pass
                    this.currentPlayer = this.secondPlayer;
                }
            }
            else if (out == this.secondPlayer) {
                if (this.currentPlayer == this.secondPlayer) {
                    this.currentPlayer = this.thirdPlayer;
                }
                else if (this.currentPlayer == this.firstPlayer) {
                    // thid player has to call next number or pass
                    this.currentSkattravelIndex++;
                    this.currentPlayer = this.thirdPlayer;
                }
                else if (this.currentPlayer == this.thirdPlayer) {
                    // first player has to match or pass 
                    this.currentPlayer = this.firstPlayer;
                }
            }
        }
        else if (passedCount == 2) {
            // notOut won skat travel
            // curretn player is first after dealer
            // this.firstPlayer won skat travel
            if (notOut == this.firstPlayer && this.currentPlayer != this.firstPlayer && this.currentSkattravelIndex == 0) {
                this.currentPlayer = this.firstPlayer;
            }
            else {
                this.io.to(this.id).emit('msg', notOut.user.username + " hat das reizen gewonnen" + this.skatTravelArray[this.currentSkattravelIndex]);
                this.currentPlayer = notOut;
                this.playingPlayer = notOut;
                this.skatTravelWinnerGetsSkat();
            }
        }
        else if (passedCount == 3) {
            this.isRamsch = true;
            this.io.to(this.id).emit('msg', 'Ist Ramsch');

        }
    }

    skatTravelPhase(socketID, bool: boolean) {
        let player = this.getPlayerWithSocketId(socketID);
        if (this.phase == 1 && player == this.currentPlayer && this.skatTravelArray[this.currentSkattravelIndex] <= player.maxSkatTravel) {
            if (bool) {
                player.currentSkatTravel = this.skatTravelArray[this.currentSkattravelIndex];
            }
            else {
                player.passedSkatTravel = true;
                this.io.to(this.id).emit('msg', this.currentPlayer.user.username + "'ist raus !");
            }
            this.skatTravelSetNextPlayer();
            this.io.to(this.id).emit('msg', this.currentPlayer.user.username + "'ist dran!");
            this.io.to(this.currentPlayer.socketID).emit('skatTravelValue', this.skatTravelArray[this.currentSkattravelIndex]);
        }
    }

    skatTravelWinnerGetsSkat() {
        let skat = this.playerMap.get(this.getPlayerWithSocketId('skat'));
        let playerCards = this.playerMap.get(this.currentPlayer);
        playerCards.push(skat[0]);
        playerCards.push(skat[1]);
        this.playerMap.set(this.getPlayerWithSocketId('skat'), []);
        this.phase++; // 2 skat drücken
        this.sendGameStateToRoom();
        this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " muss 2 Karten drücken!");
    }

    skatTravelWinnerPutSkatAway(socketID, card1, card2) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && this.phase == 2 && player == this.currentPlayer) {
            let playerCards = this.playerMap.get(player);
            if (playerCards && playerCards.includes(card1) && playerCards.includes(card2)) {
                playerCards.splice(playerCards.indexOf(card1), 1);
                playerCards.splice(playerCards.indexOf(card2), 1);
                this.playerMap.set(this.getPlayerWithSocketId('skat'), [card1, card2]);
                this.phase++;
                this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " hat 2 Karten getdrückt!");
                this.io.to(this.currentPlayer.socketID).emit('skatCards', this.playerMap.get(this.getPlayerWithSocketId('skat')));
                this.sendGameStateToRoom();
            }
        }
    }

    skatTravelWinnerConfirmGameType(socketID) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && this.phase == 3 && player == this.currentPlayer) {
            // TODO
            /* if (this.getPlayerMaxSkatTravel(player) >= this.skatTravelArray[this.currentSkattravelIndex - 1]) { */
            this.currentGameType = player.playingCardType;
            this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " spielt" + player.playingCardType);
            this.startGamePhase();
            /* } */
            /* else {
                this.io.to(socketID).emit('msg', this.currentPlayer.user.username + "  nicht möglich. Überreizt ?");
            } */
        }
    }

    // 4 game

    startGamePhase() {
        this.phase++; //4 start game
        this.currentPlayer = this.firstPlayer;
        this.sendGameStateToRoom();
    }

    isCardTrump(card) {
        if (this.currentGameType != 23 && helper.getCardValue(card) == 'U') {
            return true;
        }
        else if (this.currentGameType == 12 && helper.getCardSuit(card) == 'E') {
            return true;

        }
        else if (this.currentGameType == 11 && helper.getCardSuit(card) == 'G') {
            return true;

        }
        else if (this.currentGameType == 10 && helper.getCardSuit(card) == 'H') {
            return true;

        }
        else if (this.currentGameType == 9 && helper.getCardSuit(card) == 'S') {
            return true;

        }
        return false;

    }

    isAllowedCard(card, tablecards) {
        if (tablecards.length == 0) {
            return true;
        }
        else if ((helper.getCardSuit(card) == helper.getCardSuit(tablecards[0]) && helper.getCardValue(tablecards[0]) != 'U') || this.isCardTrump(tablecards[0]) && this.isCardTrump(card)) {
            return true;
        }
    }

    playerCantGiveSuit(player, tableCards) {
        let playerCards = this.playerMap.get(player);
        if (this.isCardTrump(tableCards[0])) {
            for (let playerCard of playerCards) {
                if (this.isCardTrump(playerCard)) {
                    return false;
                }
            }
        }
        else {
            for (let playerCard of playerCards) {
                if (helper.getCardSuit(tableCards[0]) == helper.getCardSuit(playerCard) && helper.getCardValue(playerCard) != 'U') {
                    return false;
                }
            }
        }
        return true;

    }

    isCardHigherSkat(card1: string, card2: string) {
        let card1Val = helper.getCardValue(card1);
        let card2Val = helper.getCardValue(card2);
        // check if its a number
        let card1Num = Number(card1Val);
        let card2Num = Number(card2Val);
        if (this.isCardTrump(card1) && !this.isCardTrump(card2)) {
            return true;
        }
        else if (this.isCardTrump(card1) && this.isCardTrump(card2) && !this.isRamsch && this.currentGameType != 23 && (card1Val == 'U' || card2Val != 'U')) {
            if (card1Val == 'U' && card2Val != 'U') {
                return true;
            }
            else if (card1Val != 'U' && card2Val == 'U') {
                return false;
            }
            else {
                let card1Suit = helper.getCardSuit(card1);
                let card2Suit = helper.getCardSuit(card2);
                if (card1Suit == 'E') {
                    return true;
                }
                else if (card1Suit == 'G' && card2Suit != 'E') {
                    return true;
                }
                else if (card1Suit == 'H' && !(card2Suit == 'E' || card2Suit == 'G')) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        else if (helper.getCardSuit(card1) == helper.getCardSuit(card2)) {
            if (card1Val == 'A') {
                return true;
            }
            else if (!(this.isRamsch || this.currentGameType == 23) && card1Val == '10' && card2Val != 'A') {
                return true;
            }
            else if (card1Val == 'K' && !(card2Val == 'A' || card2Val == '10')) {
                return true;
            }
            else if (card1Val == 'O' && !(card2Val == 'A' || card2Val == '10' || card2Val == 'K')) {
                return true;
            }
            else if ((this.currentGameType == 23) && card1Val == 'U' && !(card2Val == 'A' || card2Val == 'K' || card2Val == 'O')) {
                return true;
            }
            else if (card1Num > card2Num) {
                return true;
            }
            else {
                return false;
            }

        }
        else {
            return false;
        }
    }

    getHighestCard(cards) {
        let highestCard = cards[0];
        for (let card of cards) {
            if (this.isCardHigherSkat(card, highestCard)) {
                highestCard = card;
            }
        }
        return highestCard;
    }

    playerPlayCard(socketID, card) {
        let player = this.getPlayerWithSocketId(socketID);
        if (player && player == this.currentPlayer && !this.thirdCardTimeOut && this.phase == 4) {
            let playerCards = this.playerMap.get(player);
            let table = this.getPlayerWithSocketId('table');
            let tableCards = this.playerMap.get(table);
            if (playerCards.includes(card) && (this.isAllowedCard(card, tableCards) || this.playerCantGiveSuit(player, tableCards))) {
                tableCards.push(card);
                playerCards.splice(playerCards.indexOf(card), 1);
                if (tableCards.length == 3) {
                    let winnerCard = this.getHighestCard(tableCards);
                    this.io.to(this.id).emit('msg', winnerCard + "  war höchste Karte");
                    let winnerIndex = tableCards.indexOf(winnerCard);
                    if (playerCards.length == 0) {
                        this.io.to(this.id).emit('msg', "SpielEnde");
                        //TODO who won
                        this.io.to(this.id).emit('msg', this.playingPlayerStack);
                        this.countPlayerPlayingStackPoints();
                        setTimeout(() => {
                            this.start();
                        }, 10000);

                    }
                    else {
                        if (winnerIndex == 2) {
                            this.currentPlayer = this.currentPlayer;
                        }
                        else if (winnerIndex == 1) {
                            if (this.currentPlayer == this.firstPlayer) {
                                this.currentPlayer = this.thirdPlayer;
                            }
                            else if (this.currentPlayer == this.secondPlayer) {
                                this.currentPlayer = this.firstPlayer;
                            }
                            else if (this.currentPlayer == this.thirdPlayer) {
                                this.currentPlayer = this.secondPlayer;
                            }
                        }

                        else if (winnerIndex == 0) {
                            if (this.currentPlayer == this.firstPlayer) {
                                this.currentPlayer = this.secondPlayer;
                            }
                            else if (this.currentPlayer == this.secondPlayer) {
                                this.currentPlayer = this.thirdPlayer;

                            }
                            else if (this.currentPlayer == this.thirdPlayer) {
                                this.currentPlayer = this.firstPlayer;
                            }
                        }
                        if (this.currentPlayer == this.playingPlayer) {
                            this.playingPlayerStack.push(tableCards[0]);
                            this.playingPlayerStack.push(tableCards[1]);
                            this.playingPlayerStack.push(tableCards[2]);
                        }
                    }
                    this.sendGameStateToRoom();
                    this.thirdCardTimeOut = true;
                    setTimeout(() => {

                        tableCards.splice(0, 3);
                        this.thirdCardTimeOut = false;
                        this.sendGameStateToRoom();
                    }, 5000);

                }
                else {
                    if (this.currentPlayer == this.firstPlayer) {
                        this.currentPlayer = this.secondPlayer;
                    }
                    else if (this.currentPlayer == this.secondPlayer) {
                        this.currentPlayer = this.thirdPlayer;
                    }
                    else if (this.currentPlayer == this.thirdPlayer) {
                        this.currentPlayer = this.firstPlayer;
                    }
                    this.sendGameStateToRoom();
                }
            }

        }
    }

    countPlayerPlayingStackPoints() {
        let count = 0;
        for (let card of this.playingPlayerStack) {
            let cardVal = helper.getCardValue(card);
            if (cardVal == 'A') {
                count += 11;
            }
            else if (cardVal == 'K') {
                count += 4;
            }
            else if (cardVal == 'O') {
                count += 3;
            }
            else if (cardVal == 'U') {
                count += 2;
            }
            else if (cardVal == '10') {
                count += 10;
            }
        }
        if (count > 60) {
            this.io.to(this.id).emit('msg', this.playingPlayer.user.username + " gewinnt mit " + count);
            this.playingPlayer.gamePoints += this.playingPlayer.maxSkatTravel;
        }
        else {
            this.io.to(this.id).emit('msg', this.playingPlayer.user.username + " verliert mit " + count);
            this.playingPlayer.gamePoints -= this.playingPlayer.maxSkatTravel * 2;
        }
        this.sendGameStateToRoom();
    }


}
