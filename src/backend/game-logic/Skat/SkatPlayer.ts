import { IUser, User } from '../../api/models/user';
import { UserController } from '../../api/controllers/user.controller';
import { Player } from '../player';

export class SkatPlayer implements Player {
    socketID: string;
    user: IUser;
    ready = false;

    passedSkatTravel = false;
    currentSkatTravel = 0;
    maxSkatTravel: number = 0;

    playingCardType: number = 12; // Shell (9),Red (10),Green (11),Acorn (12), NullGame (23), Grand (24)
    playingHand: number = 0;
    playingOuvert: number = 0

    gamePoints = 0;

    constructor(socketID) {
        this.socketID = socketID;
    }

    async getUserWithSocketId() {
        return this.user = await new UserController().getUserWithSocketId(this.socketID);
    }

    setplayingCardType(num: number) {
        this.playingCardType = num;
    }

    togglePlayingHand() {
        if (this.playingHand == 0) {
            this.playingHand = 1;
        }
        else {
            this.playingHand = 0;
        }
    }

    toggelPlayingOuvert() {
        if (this.playingOuvert == 0) {
            this.playingOuvert = 1;
        }
        else {
            this.playingOuvert = 0;
        }
    }


}