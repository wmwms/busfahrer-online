import { IUser, User } from '../../api/models/user';
import { UserController } from '../../api/controllers/user.controller';
import { Player } from '../player';

export class BusfahrerSaxonyPlayer implements Player {
    socketID;
    user: IUser;
    ready: boolean = false;
    round = 0;
    drinks = 0;
    canSendAmmount = 0;
    skip = false;

    constructor(socketID) {
        this.socketID = socketID;
    }

    async getUserWithSocketId() {
        return this.user = await new UserController().getUserWithSocketId(this.socketID);
    }

    readyUp() {
        this.ready = true;
    }

    async receiveSip(ammount: number) {
        this.drinks += ammount;
        this.user.gameStats.busfahrerStats.lifeTimeReceivedSips += ammount;
        this.user.gameStats.busfahrerStats.sipsRatio = parseFloat((this.user.gameStats.busfahrerStats.lifeTimeSentSips / this.user.gameStats.busfahrerStats.lifeTimeReceivedSips).toFixed(2));
        await User.findOneAndUpdate({ currentSocketId: this.socketID }, { gameStats: this.user.gameStats });
    }

    async sendSip(ammount: number) {
        this.canSendAmmount -= ammount;
        this.user.gameStats.busfahrerStats.lifeTimeSentSips += ammount;
        this.user.gameStats.busfahrerStats.sipsRatio = parseFloat((this.user.gameStats.busfahrerStats.lifeTimeSentSips / this.user.gameStats.busfahrerStats.lifeTimeReceivedSips).toFixed(2));
        await User.findOneAndUpdate({ currentSocketId: this.socketID }, { gameStats: this.user.gameStats });
    }

    async updateGamesPlayed() {
        if (!this.user.gameStats.busfahrerStats.gamesPlayed) {
            this.user.gameStats.busfahrerStats.gamesPlayed = 0;
        }
        this.user.gameStats.busfahrerStats.gamesPlayed += 1;
        await User.findOneAndUpdate({ currentSocketId: this.socketID }, { gameStats: this.user.gameStats });

    }
}