import * as helper from '../../util/helperFunctions';
import { Game } from '../game';
import { Socket, Server as SocketIoServer } from "socket.io";
import { BusfahrerSaxonyPlayer } from './BusFahrerSaxonyPlayer';

export class BusfahrerSaxonyGame implements Game {
    id: string;
    io: SocketIoServer;
    playerMap: Map<BusfahrerSaxonyPlayer, string[]> = new Map();
    maxPlayerCount = 9;
    phase: number = 0;
    hasStarted = false;
    firstPlayer: BusfahrerSaxonyPlayer;
    currentPlayer: BusfahrerSaxonyPlayer;
    pyramidTimeOut;
    busCardArr: string[] = [];
    busPathArr: number[] = [];
    busFalseGuess: boolean = false;
    timeBetweenRoundsSec: number = 20;
    timeBetweenBusRefillSec: number = 1;

    constructor(id, io: SocketIoServer) {
        this.id = id;
        this.io = io;
    }

    async playerJoin(socketId) {
        let player = new BusfahrerSaxonyPlayer(socketId);
        if (!this.hasStarted) {
            if (await player.getUserWithSocketId()) {
                if (player.user.username == null || /^\s*$/.test(player.user.username)) {
                    this.io.to(socketId).emit('msg', 'You will have to set a valid Username before you can ready up!');
                    this.io.to(this.id).emit('msg', "New Player joined!");
                }
                else {
                    this.io.to(this.id).emit('msg', player.user.username + " joined!");
                }
            }
        }
        else {
            this.io.to(socketId).emit('msg', "Game already in progress. But you can watch");
            this.sendGameStateToSocket(socketId);
        }
    }

    // rm player from playermap
    playerLeave(socketId) {
        let player = this.getPlayerWithSocketId(socketId);
        // TODO put cards back in deck;
        if (player) {
            if (this.currentPlayer && this.currentPlayer.socketID == socketId && this.phase < 5) {
                this.setNextPlayerGuessingPhase();
            }
            if (this.firstPlayer && this.currentPlayer && this.firstPlayer.socketID == socketId && this.phase < 5) {
                this.firstPlayer = this.currentPlayer;
            }
            this.playerMap.delete(player);
            this.sendGameStateToRoom();
            return true
        }
        return false;
    }

    // set player as ready
    async playerReady(socketId) {
        if (!this.hasStarted && (this.getPlayerWithSocketId(socketId) == null || !this.getPlayerWithSocketId(socketId).ready)) {
            let player = new BusfahrerSaxonyPlayer(socketId);
            if (player && await player.getUserWithSocketId()) {
                if (player.user.username == null || /^\s*$/.test(player.user.username)) {
                    this.io.to(socketId).emit('msg', 'You will have to set a valid Username before playing!');
                }
                else if (this.getPlayerCount() < this.maxPlayerCount && this.playerMap.get(player) == null) {
                    this.playerMap.set(player, []);
                    player.ready = true;
                    this.io.to(this.id).emit('msg', player.user.username + " is ready!" + this.getPlayerReadyCount() + "/" + await this.getClientSocketCount());

                }
            }
            else {
                this.io.to(socketId).emit('msg', 'Something went wrong getting player. Check if multiple Tabs are open and make sure to only have one open.');
            }

        }
        if (await this.getClientSocketCount() == this.getPlayerReadyCount() || this.getPlayerReadyCount() == 9) {
            this.start();
        }
    }

    getPlayerCount(): number {
        let count = 0;
        for (let player of this.playerMap.keys()) {
            if (player.socketID != "deck" && player.socketID != "pyramid") {
                count++;
            }
        }
        return count;
    }

    async getClientSocketCount(): Promise<number> {
        return await (await this.io.in(this.id).allSockets()).size;
    }

    getPlayerReadyCount(): number {
        let count = 0;
        for (let player of this.playerMap.keys()) {
            if (player.socketID != "deck" && player.socketID != "pyramid" && player.ready) {
                count++;
            }
        }
        return count;
    }

    getPlayerWithSocketId(socketID) {
        for (let player of this.playerMap.keys()) {
            if (player.socketID == socketID) {
                return player;
            }
        }
        return null;
    }



    sendGameStateToSocket(socketID) {
        if (this.hasStarted) {
            this.io.to(socketID).emit('phase', this.phase);
            if (this.getPlayerWithSocketId(socketID) != null) {
                this.io.to(socketID).emit('ownCards', this.getPublicPlayerCards(socketID));
            }
            if (this.phase >= 5 && this.phase < 16) {
                this.io.to(socketID).emit('pyramidArray', this.getPyramidWithPhase());
            }
            if (this.phase >= 16) {
                this.io.to(socketID).emit('busArray', this.getBusArray());
            }
            this.io.to(socketID).emit('playerMap', JSON.stringify([...this.getPublicPlayerMap()]));
        }
    }

    sendGameStateToRoom() {
        if (this.hasStarted) {
            this.io.to(this.id).emit('phase', this.phase);
            for (let player of this.playerMap.keys()) {
                this.io.to(player.socketID).emit('selfUpdate', this.getPublicPlayerCards(player.socketID), player);
            }
            if (this.phase >= 5 && this.phase < 16) {
                this.io.to(this.id).emit('pyramidArray', this.getPyramidWithPhase());
            }
            if (this.phase >= 16) {
                this.io.to(this.id).emit('busArray', this.getBusArray());
            }
            this.io.to(this.id).emit('playerMap', JSON.stringify([...this.getPublicPlayerMap()]));
            if (this.phase < 5 && this.currentPlayer && this.currentPlayer.user) {
                this.io.to(this.id).emit('msg', "It is " + this.currentPlayer.user.username + "'s turn");
            }
        }
    }

    playerSendSips(fromSocketID, ammount, toSocketID) {
        let toPlayer = this.getPlayerWithSocketId(toSocketID);
        let fromPlayer = this.getPlayerWithSocketId(fromSocketID);
        if (fromPlayer && toPlayer && fromPlayer.canSendAmmount >= ammount && ammount > 0) {
            fromPlayer.sendSip(ammount);
            toPlayer.receiveSip(ammount);
            this.io.to(this.id).emit('msg', fromPlayer.user.username + " send " + ammount + " sips to " + toPlayer.user.username);
            this.io.to(toPlayer.socketID).emit('receiveDrinks', ammount, fromPlayer.user.username);
            this.sendGameStateToRoom();

        }
        else if (fromPlayer) {
            this.io.to(fromPlayer.socketID).emit('msg', 'Thats not legal.');
        }
    }

    start() {
        this.io.to(this.id).emit('msg', 'Game is starting');
        this.hasStarted = true;
        this.createNewPlayerMap();
        this.sendGameStateToRoom();
    }

    restart() {
        this.io.to(this.id).emit('msg', "Game will restart in " + this.timeBetweenRoundsSec + " sec");
        for (let player of this.playerMap.keys()) {
            if (player.socketID != "deck" && player.socketID != "pyramid" && player.user) {
                player.updateGamesPlayed();
            }
        }
        setTimeout(() => {
            this.playerMap = new Map();
            clearTimeout(this.pyramidTimeOut);
            this.phase = 0;
            this.hasStarted = false;
            this.io.to(this.id).emit('phase', -1);
            this.sendGameStateToRoom();
        }, this.timeBetweenRoundsSec * 1000);
    }

    createNewPlayerMap() {
        // get all 51 Cards in random order
        let cards = helper.create2ADeck();
        // add all cards to deck
        this.playerMap.set(new BusfahrerSaxonyPlayer("deck"), cards);


        let pyramidCards = [];
        //set 10 cards to pyramid
        for (let i = 0; i < 10; i++) {
            pyramidCards.push(cards[i]);
        }
        this.playerMap.set(new BusfahrerSaxonyPlayer("pyramid"), pyramidCards);

        // set 4 cards to every player
        let i = 10;
        let isFirst = true;
        this.playerMap.forEach((value: string[], key: BusfahrerSaxonyPlayer) => {
            if (key.socketID != "deck" && key.socketID != "pyramid") {
                if (isFirst) {
                    this.firstPlayer = key;
                    isFirst = false;
                }
                this.playerMap.set(key, [cards[i], cards[i + 1], cards[i + 2], cards[i + 3]]);
                i = i + 4;
            }

        });
        // set first player 
        this.currentPlayer = this.firstPlayer;
        return this.playerMap;
    }

    /* Guessing Phase */

    // set current player to next or  first if round is over
    setNextPlayerGuessingPhase() {
        let isNext = false;
        let nextFound = false;
        for (let key of this.playerMap.keys()) {
            if (key.socketID != "deck" && key.socketID != "pyramid") {
                if (isNext) {
                    this.currentPlayer = key;
                    nextFound = true;
                    break; // foreach wont break ...
                }
                // for will break on nex player;
                if (key == this.currentPlayer) {
                    isNext = true;
                }
            }
        }
        // if no next player found, return first and increase phase
        if (!nextFound) {
            this.phase++;
            this.currentPlayer = this.firstPlayer;
            if (this.phase == 4) {
                this.startPyramidPhase();
                this.currentPlayer = null;
            }
        }

        return;
    }

    // currentPlayer guesses, return true on right guess, false on false guess, TODO throw exception if other player cant wait for their turn (catch in socketserver)
    playerGuess(socketId, round, value) {
        if (this.hasStarted && this.phase < 5) {
            if (this.getPlayerWithSocketId(socketId) == this.currentPlayer) {
                let playerCards = this.playerMap.get(this.currentPlayer);
                let result = false;
                let oddsRule = false;
                if (playerCards && this.phase == 0 && round == "redOrBlack") {
                    result = helper.isCardSameColor(value, playerCards[0]);
                }
                else if (playerCards && this.phase == 1 && round == "highOrLow") {
                    // same card value = false for high or low
                    if (helper.getCardValue(playerCards[0]) == helper.getCardValue(playerCards[1])) {

                    }
                    else if (value == 'high' && helper.isCardHigher(playerCards[1], playerCards[0])) {
                        result = true;
                    }
                    else if (value == 'low' && !helper.isCardHigher(playerCards[1], playerCards[0])) {
                        result = true;
                    }
                }
                else if (playerCards && this.phase == 2 && round == "betweenOrNot") {
                    if (value == 'true' && helper.isCardBetween(playerCards[2], playerCards[0], playerCards[1])) {
                        result = true;
                    }
                    else if (value == 'false' && !helper.isCardBetween(playerCards[2], playerCards[0], playerCards[1])) {
                        result = true;
                    }
                }
                else if (playerCards && this.phase == 3 && round == "containsSuitOrNot") {
                    //TODO if 4 diffrent Suits and guesse correct -> everyone drinks
                    if (value == 'true' && helper.isCardSuitAlreadyInCards(playerCards[3], [playerCards[0], playerCards[1], playerCards[2]])) {
                        result = true;
                        oddsRule = true;
                        for (let card of playerCards) {
                            for (let card2 of playerCards) {
                                if (card != card2 && helper.getCardSuit(card) != helper.getCardSuit(card2)) {
                                    oddsRule = false;
                                }
                            }
                        }
                    }
                    else if (value == 'false' && !helper.isCardSuitAlreadyInCards(playerCards[3], [playerCards[0], playerCards[1], playerCards[2]])) {
                        result = true;
                        oddsRule = true;
                        for (let card of playerCards) {
                            for (let card2 of playerCards) {
                                if (card != card2 && helper.getCardSuit(card) == helper.getCardSuit(card2)) {
                                    oddsRule = false;
                                }
                            }
                        }
                    }

                }
                else {
                    return null;
                }
                this.currentPlayer.round++;
                if (result) {
                    if (!oddsRule) {
                        this.currentPlayer.canSendAmmount += this.currentPlayer.round;
                        this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " guessed " + round + " with " + value + " correctly.");
                        this.io.to(socketId).emit('guess', result, this.currentPlayer.canSendAmmount);
                    }
                    else {
                        for (let player of this.playerMap.keys()) {
                            if (player.socketID != this.currentPlayer.socketID) {
                                this.playerSendSips(this.currentPlayer.socketID, this.currentPlayer.round, player.socketID);
                            }
                        }
                        this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " guessed " + round + " with " + value + " correctly. Odd Rule applies everyone has to drink!");
                    }

                }
                else {
                    this.currentPlayer.receiveSip(this.currentPlayer.round);
                    this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " guessed " + round + " with " + value + " incorrectly. Gotta drink " + this.currentPlayer.round);
                    this.io.to(socketId).emit('guess', result, this.currentPlayer.round);
                }
                this.setNextPlayerGuessingPhase();
                this.sendGameStateToRoom();
            }
            else {
                this.io.to(socketId).emit('msg', 'Its not your Turn!');
            }
        }
    }



    // Get own Cards and hide those not visible yet
    getPublicPlayerCards(socketId) {
        let player = this.getPlayerWithSocketId(socketId);
        if (player) {
            let valueArr: Array<string> = JSON.parse(JSON.stringify(this.playerMap.get(player))); // deep copy so original does not get changed 
            for (let cardVal of valueArr) {
                if (valueArr.indexOf(cardVal) >= player.round) {
                    valueArr[valueArr.indexOf(cardVal)] = "card_back";
                }
            }
            return valueArr;
        }
        return null;
    }

    // Get All Cards of Players and hide those not visible yet
    getPublicPlayerMap() {
        let returnMap: Map<any, string[]> = new Map<any, string[]>();
        this.playerMap.forEach((value: string[], key: BusfahrerSaxonyPlayer) => {
            if (key.socketID != "deck" && key.socketID != "pyramid") {
                let active = false;
                if (this.currentPlayer == key) {
                    active = true;
                }
                let pubKey = { username: key.user.username, socketID: key.socketID, drinks: key.drinks, active: active }
                let valueArr = JSON.parse(JSON.stringify(value)); // deep copy so original does not get changed
                for (let cardVal of valueArr) {
                    if (valueArr.indexOf(cardVal) >= key.round) {
                        valueArr[valueArr.indexOf(cardVal)] = "card_back";
                    }
                }
                returnMap.set(pubKey, valueArr);
            }
        });
        return returnMap;
    }


    /* Pyramid Phase */
    startPyramidPhase() {
        this.io.to(this.id).emit('msg', 'Game will continue in ' + this.timeBetweenRoundsSec + ' sec');
        this.phase++;
        this.restartPyramidNextCardTimeOut();
    }

    restartPyramidNextCardTimeOut() {
        clearTimeout(this.pyramidTimeOut);
        this.pyramidTimeOut = setTimeout(() => {
            this.phase++;
            this.sendGameStateToRoom();
            if (this.phase >= 16) {
                clearTimeout(this.pyramidTimeOut);
                if (this.phase == 16) {
                    this.startBusPhase();
                }
            }
            else {
                this.restartPyramidNextCardTimeOut();
            }
        }, this.timeBetweenRoundsSec * 1000);
    }

    playerSkipWaitNextCardPyramidPhase(socketID) {
        if (this.phase >= 5 && this.phase <= 16) {
            let player = this.getPlayerWithSocketId(socketID);
            if (player) {
                player.skip = true;
                let playerSkipCount = 0;
                for (let player of this.playerMap.keys()) {
                    if (player.socketID != "deck" && player.socketID != "pyramid" && player.skip) {
                        playerSkipCount++;
                    }
                }
                this.io.to(this.id).emit('msg', player.user.username + " wants to skip waiting. " + (playerSkipCount + "/" + this.getPlayerCount()));

                if (this.getPlayerCount() == playerSkipCount) {
                    clearTimeout(this.pyramidTimeOut);
                    this.phase++;
                    this.sendGameStateToRoom();
                    if (this.phase == 16) {
                        this.startBusPhase();
                    }
                    else {
                        this.io.to(this.id).emit('msg', "Skiping to next Card, next Card in " + this.timeBetweenRoundsSec + 'sec');
                        this.restartPyramidNextCardTimeOut();
                    }
                    for (let player of this.playerMap.keys()) {
                        if (player.socketID != "deck" && player.socketID != "pyramid") {
                            player.skip = false;
                        }
                    }
                }
            }
        }

    }

    // Get Pyramid Cards and hide those not visible yet
    getPyramidWithPhase() {
        let valueArr = [];
        this.playerMap.forEach((value: string[], key: BusfahrerSaxonyPlayer) => {
            if (key.socketID == "pyramid") {
                valueArr = JSON.parse(JSON.stringify(value)); // deep copy so original does not get changed
                for (let cardVal of valueArr) {
                    if (valueArr.indexOf(cardVal) >= this.phase - 5) {
                        valueArr[valueArr.indexOf(cardVal)] = "card_back";
                    }
                }
            }
        });
        return valueArr;
    }

    getPyramidActiveCard() {
        let activeCard;
        this.playerMap.forEach((value: string[], key: BusfahrerSaxonyPlayer) => {
            if (key.socketID == "pyramid") {
                activeCard = value[this.phase - 6];
            }
        });
        return activeCard;
    }

    playerPutCardOnPyramid(socketId, card) {
        if (this.phase >= 6) {
            let player = this.getPlayerWithSocketId(socketId);
            let playersCard = this.playerMap.get(player);
            let activeCard = this.getPyramidActiveCard();
            if (player && playersCard && helper.getCardValue(activeCard) == helper.getCardValue(card)) {
                playersCard.splice(playersCard.indexOf(card), 1);
                this.playerMap.set(player, playersCard);
                let pos = this.getPyramidWithPhase().indexOf(this.getPyramidActiveCard());
                if (pos >= 0 && pos < 4) {
                    player.canSendAmmount += 1;
                }
                else if (pos >= 4 && pos < 7) {
                    player.canSendAmmount += 2;
                }
                else if (pos >= 7 && pos < 9) {
                    player.canSendAmmount += 3;
                }
                else if (pos == 9) {
                    player.canSendAmmount += 4;
                }
                this.io.to(this.id).emit('msg', player.user.username + " put Card on Pyramid.(" + card + ")");
                this.io.to(socketId).emit('putCardOnPyramid', true, player.canSendAmmount);
                this.sendGameStateToRoom();
            }
            else {
                this.io.to(socketId).emit('msg', 'Not valid');
            }
        }
    }

    /* Bus Phase */
    startBusPhase() {
        this.busCardArr = [];
        this.busPathArr = [];
        this.busFalseGuess = false;
        let loser = this.setLoserAsCurrentPLayer();
        if (loser) {
            this.io.to(this.id).emit('msg', loser.user.username + ' is the bus driver. gl hf');
            this.io.to(this.id).emit('msg', 'Game will continue in ' + this.timeBetweenRoundsSec + ' sec ');
            setTimeout(() => {
                this.phase++;
                this.sendGameStateToRoom();
            }, this.timeBetweenRoundsSec * 1000);
        }
        else {
            this.io.to(this.id).emit('msg', 'Everyone got rid of their cards. Nobody is the loser. Game finished');
            this.restart();
        }
    }

    setLoserAsCurrentPLayer() {
        let loser: BusfahrerSaxonyPlayer;
        let losingCards = [];
        this.playerMap.forEach((value: string[], key: BusfahrerSaxonyPlayer) => {
            if (value.length != 0 && key.socketID != "deck" && key.socketID != "pyramid") {
                if (value.length > losingCards.length) {
                    loser = key;
                    losingCards = value;
                }
                else if (value.length == losingCards.length) {
                    let losingCardsArr = JSON.parse(JSON.stringify(losingCards));
                    let valueArr = JSON.parse(JSON.stringify(value));
                    for (let i = 0; i < 4; i++) {
                        let lowestLosing = helper.findLowestCard(losingCards);
                        let lowestValue = helper.findLowestCard(value);

                        if (helper.isCardHigher(lowestLosing, lowestValue)) {
                            loser = key;
                            losingCards = value;
                            break;
                        }
                        else if (helper.getCardValue(lowestLosing) == helper.getCardValue(lowestValue)) {
                            losingCardsArr.splice(losingCards.indexOf(lowestLosing));
                            valueArr.splice(valueArr.indexOf(lowestValue));
                        }
                    }
                }
            }
        });
        this.currentPlayer = loser;
        return this.currentPlayer;
    }

    getBusArray() {
        if (!this.busCardArr || this.busCardArr.length < 14) {
            this.busCardArr = helper.create2ADeck();
        }
        let publicBusArr = JSON.parse(JSON.stringify(this.busCardArr));
        for (let i = 0; i < this.busCardArr.length; i++) {
            if (!this.busPathArr.includes(i)) {
                publicBusArr[i] = "card_back";
            }
        }
        return publicBusArr;
    }

    // first row, second row usw
    isValidBusGuess(pos) {
        if (this.busPathArr.length == 0 && (pos >= 0 && pos < 2)) {
            return true;
        }
        else if (this.busPathArr.length == 1 && (pos >= 2 && pos < 5)) {
            return true;
        }
        else if (this.busPathArr.length == 2 && (pos >= 5 && pos < 9)) {
            return true;
        }
        else if (this.busPathArr.length == 3 && (pos >= 9 && pos < 12)) {
            return true;
        }
        else if (this.busPathArr.length == 4 && (pos >= 12 && pos < 14)) {
            return true;
        }
        else {
            return false;
        }
    }

    playerGuessBusPhase(socketId, pos) {
        let result;
        if (this.currentPlayer && socketId == this.currentPlayer.socketID && this.phase >= 16 && !this.busFalseGuess && this.isValidBusGuess(pos)) {
            if (this.getPlayerWithSocketId(socketId) == this.currentPlayer && !this.busFalseGuess) {
                let cardVal = helper.getCardValue(this.busCardArr[pos]);
                this.busPathArr.push(pos);
                if (cardVal == 'J' || cardVal == 'Q' || cardVal == 'K' || cardVal == 'A') {
                    this.busFalseGuess = true;
                    result = false;
                }
                else {
                    result = true;
                }
            }
            if (result == true) {
                this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " guessed correctly.");
                this.sendGameStateToRoom();
                if (this.busPathArr.length == 5) {
                    this.io.to(this.id).emit('msg', 'Game is finished');
                    this.restart();
                }
            }
            else {
                this.currentPlayer.receiveSip(this.busPathArr.length);
                this.io.to(this.id).emit('msg', this.currentPlayer.user.username + " guessed incorrectly. Gotta Drink " + this.busPathArr.length + ". Try again in " + this.timeBetweenBusRefillSec + " sec");
                this.io.to(socketId).emit('guess', false, this.busPathArr.length);
                this.sendGameStateToRoom();
                setTimeout(() => {
                    this.refillBus();
                    this.sendGameStateToRoom();
                }, this.timeBetweenBusRefillSec * 1000);
            }
        }
        else {
            this.io.to(socketId).emit('msg', "Not valid");
        }
    }

    refillBus() {
        for (let pos of this.busPathArr) {
            this.busCardArr.splice(pos, 1);
        }
        this.busPathArr = [];
        this.busFalseGuess = false;
    }

}