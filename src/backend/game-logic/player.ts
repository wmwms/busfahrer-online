import { UserController } from '../api/controllers/user.controller'
import { PlayerGameStats } from '../api/models/gameStats';
import { IUser, User } from '../api/models/user';
export interface Player {
    socketID: string;
    user: IUser;

    getUserWithSocketId();
}

