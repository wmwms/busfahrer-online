import { Player } from './player';
import { Socket, Server as SocketIoServer } from "socket.io";

export interface Game {
    id: string;
    io: SocketIoServer;
    playerMap: Map<Player, any>;
    maxPlayerCount: number;

    playerJoin(socketID);

    playerLeave(socketID);

    playerReady(socketID);

    getPlayerWithSocketId(socketID);

    getPlayerCount(): number;
    getClientSocketCount(): Promise<number>;

    start();

    sendGameStateToSocket(socketID);

    sendGameStateToRoom();
}


