export function shuffleCards(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

// creates deck with cards from 2 to Ace
export function create2ADeck() {
    let suits = ["P", "K", "C", "H"];
    let cardtypes = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    let allCards = [];
    for (let suit of suits) {
        for (let cardtype of cardtypes) {
            allCards.push(cardtype + suit);
        }
    }
    return shuffleCards(allCards);
}

// creates deck with cards from 7 to Ace
export function create7ADeck() {
    let suits = ["P", "K", "C", "H"];
    let cardtypes = ["7", "8", "9", "10", "J", "Q", "K", "A"];
    let allCards = [];
    for (let suit of suits) {
        for (let cardtype of cardtypes) {
            allCards.push(cardtype + suit);
        }
    }
    return shuffleCards(allCards);
}

export function create7ADeckGerman() {
    let suits = ["E", "G", "H", "S"];
    let cardtypes = ["7", "8", "9", "10", "U", "O", "K", "A"];
    let allCards = [];
    for (let suit of suits) {
        for (let cardtype of cardtypes) {
            allCards.push(cardtype + suit);
        }
    }
    return shuffleCards(allCards);
}

// card => 'Val|Suit' ('2C')
export function getCardValue(card: string): string {
    // if value is 10
    if (card.length == 3) {
        return card[0] + card[1];
    }
    return card[0];
}

export function getCardSuit(card): string {
    // if value is 10
    if (card.length == 3) {
        return card[2];
    }
    return card[1];
}

// color 'black' or 'red'
export function isCardSameColor(color: string, card: string) {
    let cardSuit = this.getCardSuit(card);
    if (color == 'black' && (cardSuit == 'P' || cardSuit == 'C')) {
        return true;
    }
    else if (color == 'red' && (cardSuit == 'H' || cardSuit == 'K')) {
        return true;
    }
    return false;
}

// returns true if card1 > card2
export function isCardHigher(card1: string, card2: string) {
    let card1Val = this.getCardValue(card1);
    let card2Val = this.getCardValue(card2);
    // check if its a number
    let card1Num = Number(card1Val);
    let card2Num = Number(card2Val);
    // false if same, or 1. card is number and 2. card is not (J,Q,K)
    if (card1Val == card2Val || (card1Num && !card2Num)) {
        return false;
    }
    // true if first card not number (J,Q,K) and second is, or both are numbers but first is higher
    else if ((!card1Num && card2Num) || (card1Num && card2Num && card1Num > card2Num)) {
        return true;
    }
    // true if first A (second also A already checked above)
    else if (card1Val == 'A') {
        return true
    }
    // true if first K and second Q or J (second also K already checked above)
    else if (card1Val == 'K' && (card2Val == 'Q' || card2Val == 'J')) {
        return true
    }

    // true if first Q and second J  (second also J already checked above)
    else if (card1Val == 'Q' && card2Val == 'J') {
        return true
    }
    // first J akready checked in 1. (J == J) and 2. (J > number)   
    return false;
}

// returns true if card1 is between card2 and 3
export function isCardBetween(card1: string, card2: string, card3: string) {
    let card1Val = this.getCardValue(card1);
    let card2Val = this.getCardValue(card2);
    let card3Val = this.getCardValue(card3);
    // is between if same value
    if (card1Val == card2Val || card1Val == card3Val) {
        return true;
    }
    // is between for  card2 < card1 < card3 , meaning card1 is higher card2 and card1 not higher card3
    if (this.isCardHigher(card1, card2) && !this.isCardHigher(card1, card3)) {
        return true;
    }
    // is between for  card3 < card1 < card2 , meaning card1 is higher card3 and card1 not higher card2
    if (this.isCardHigher(card1, card3) && !this.isCardHigher(card1, card2)) {
        return true;
    }
    return false;
}


// returns true if suit of card already in other3Cards 
export function isCardSuitAlreadyInCards(card: string, other3Cards: [string, string, string]) {
    let cardSuit = this.getCardSuit(card);
    let card2Suit = this.getCardSuit(other3Cards[0]);
    let card3Suit = this.getCardSuit(other3Cards[1]);
    let card4Suit = this.getCardSuit(other3Cards[2]);
    if (cardSuit == card2Suit || cardSuit == card3Suit || cardSuit == card4Suit) {
        return true;
    }
    return false;
}

export function findLowestCard(cards: string[]) {
    let lowestCard = cards[0];
    for (let card of cards) {
        // new lowest Card of this hand if lowestCard higher
        if (this.isCardHigher(lowestCard, card)) {
            lowestCard = card;
        }
    }
    return lowestCard;
}