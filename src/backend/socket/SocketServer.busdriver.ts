import { BusfahrerSaxonyGame } from "../game-logic/BusFahrerSaxony/BusFahrerSaxonyGame";

export function busDriverSocketServer(socket, games) {
    socket.on('guess', (roomId: string, round: string, value: string) => {
        let game = <BusfahrerSaxonyGame>games.get(roomId);
        if (game) {
            game.playerGuess(socket.id, round, value);
        }
    });

    socket.on('nextCard', (roomId: string) => {
        let game = <BusfahrerSaxonyGame>games.get(roomId);
        if (game) {
            game.playerSkipWaitNextCardPyramidPhase(socket.id);

        }
    })

    socket.on('putCardOnPyramid', (roomId: string, card: string) => {
        let game = <BusfahrerSaxonyGame>games.get(roomId);
        if (game) {
            game.playerPutCardOnPyramid(socket.id, card);

        }
    });

    socket.on('guessBus', (roomId: string, position: string) => {
        let game = <BusfahrerSaxonyGame>games.get(roomId);
        if (game) {
            game.playerGuessBusPhase(socket.id, position);
        }
    });

    socket.on('sendDrinks', (roomId: string, ammount: number, toSocketID: any) => {
        let game = <BusfahrerSaxonyGame>games.get(roomId);
        if (game) {
            game.playerSendSips(socket.id, ammount, toSocketID);
        }
    });
}

