import { SkatGame } from "../game-logic/Skat/SkatGame";

export function skatSocketServer(socket, games) {
    socket.on('setPlayingTypeValue', (roomid: string, value: number) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.playerSetPlayingTypeValue(socket.id, value);
        }
    });
    socket.on('toggleHand', (roomid: string) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.playerToggleHand(socket.id);
        }
    });
    socket.on('toggleOuvert', (roomid: string) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.playerToggleOuvert(socket.id);
        }
    });

    socket.on('skatTravelAnswer', (roomid: string, value: boolean) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.skatTravelPhase(socket.id, value);
        }
    });

    socket.on('putSkatAway', (roomid: string, card1: string, card2: string) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.skatTravelWinnerPutSkatAway(socket.id, card1, card2);
        }
    });

    socket.on('setGameType', (roomid: string) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.skatTravelWinnerConfirmGameType(socket.id);
        }
    });

    socket.on('playCard', (roomid: string, card: string) => {
        let game = <SkatGame>games.get(roomid);
        if (game) {
            game.playerPlayCard(socket.id, card);
        }
    });
}
