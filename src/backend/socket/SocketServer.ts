import { Server } from 'http';
import { Socket, Server as SocketIoServer } from "socket.io";
import { busDriverSocketServer } from './SocketServer.busdriver';
import { skatSocketServer } from './SocketServer.skat';
import { Game } from '../game-logic/game';
import { BusfahrerSaxonyGame } from '../game-logic/BusFahrerSaxony/BusFahrerSaxonyGame';
import { SkatGame } from '../game-logic/Skat/SkatGame';

export class SocketServer {
    private io: SocketIoServer;
    games: Map<string, Game> = new Map();
    constructor(server: Server) {
        this.io = new SocketIoServer(server, {
            cors: {
                origin: "http://localhost:4200",
                methods: ["GET", "POST"],
                credentials: true
            }
        });

        this.io.on("connection", (socket: Socket) => {
            console.log("Sockets => " + socket.id + " connected");
            socket.emit("connection");

            socket.on("disconnect", async () => {
                this.clearRooms(socket);
                console.log("Sockets => " + socket.id + " disconnected");
            });


            //TODO option for other game modes
            socket.on("join", (roomId: string, gametype: string) => {
                // leave all other rooms before joining new
                this.clearRooms(socket);
                socket.join(roomId);
                let game = this.games.get(roomId);
                if (!game) {
                    if (gametype == 'busdriver') {
                        this.games.set(roomId, new BusfahrerSaxonyGame(roomId, this.io));
                    }
                    else if (gametype == 'skat') {
                        this.games.set(roomId, new SkatGame(roomId, this.io));

                    }
                    console.log("Created Room " + roomId);
                    game = this.games.get(roomId);
                }
                game.playerJoin(socket.id);
            });

            socket.on("ready", (roomId: string) => {
                let game = this.games.get(roomId);
                if (game) {
                    game.playerReady(socket.id);
                }
            });

            // update single socket
            socket.on("getupdate", (roomId: string) => {
                let game = this.games.get(roomId);
                if (game) {
                    game.sendGameStateToSocket(socket);
                }
            });

            busDriverSocketServer(socket, this.games);
            skatSocketServer(socket, this.games);

        });
    }

    async clearRooms(socket: Socket) {
        // clear socket rooms 
        for (let room of socket.rooms) {
            // dont delte the sockets unique room
            if (room != socket.id) {
                socket.leave(room);
            }
        }
        // remove player from game
        for (let room of this.games.keys()) {
            let game = this.games.get(room);
            if (game.playerLeave(socket.id)) {
                // delete room if no socket is connected
                if (await game.getClientSocketCount() == 0) {
                    console.log("Deleted Room " + room);
                    this.games.delete(room);
                }
            };

        }
    }
}