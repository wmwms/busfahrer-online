import { Request, Response } from "express";
import { User, IUser } from "../models/user";
import * as jwt from 'jsonwebtoken';
import { jwtSecret } from '../../util/secret';
import { PlayerGameStats } from '../models/gameStats';


export class UserController {

    public async newUser(req: Request, res: Response): Promise<void> {
        console.log("Api => Register");
        let newUser: IUser = new User();
        newUser.gameStats = new PlayerGameStats();
        let user = await newUser.save();
        let token = jwt.sign({ user }, jwtSecret, { expiresIn: '30d' });
        console.log("Success : user created");
        let returnUser = {
            username: user.username,
            created_date: user.created_date
        }
        res.status(200).send({ success: true, token: token, user: returnUser });
    }

    public async getUser(req: Request, res: Response) {
        console.log("Api => Login");
        let user = await User.findOne({ _id: res.locals.id });
        if (!user) {
            console.log("Failed : doesnt exist");
            res.status(201).send({ success: false, message: "Not Valid" });
        }
        else {
            let token = jwt.sign({ user }, jwtSecret, { expiresIn: '30d' });
            let returnUser = {
                username: user.username,
                created_date: user.created_date,
                currentSocketId: user.currentSocketId
            }
            res.status(200).send({ success: true, token: token, user: returnUser });
        }

    }

    public async updateUser(req: Request, res: Response) {
        console.log("Api => UpdateUser");
        await User.findByIdAndUpdate(res.locals.id, req.body, { new: true }, (err: any, user: IUser) => {
            if (user) {
                let token = jwt.sign({ user }, jwtSecret, { expiresIn: '30d' });
                let returnUser = {
                    username: user.username,
                    created_date: user.created_date,
                }
                // will return old user document.....
                res.status(200).send({ success: true, token: token, user: returnUser });
            }
        });

    }

    public async getUserWithSocketId(socketID: string) {
        console.log("Api => Find User with socketId");
        return await User.findOne({ currentSocketId: socketID }, (err, user) => {
            if (err) {
                console.log(err);
            }
            else if (!user) {
                console.log("Failed : doesnt exist");

            }
            else {
            }
        });
    }
}