import { Request, Response } from "express";
import { User } from "../models/user";

export class GameStatController {
    // Busfahrer
    public async rankListSipsReceived(req: Request, res: Response) {
        User.find({}).sort({ 'gameStats.busfahrerStats.lifeTimeReceivedSips': -1 }).limit(10).exec(function (err, users) {
            if (users) {
                let filteredUsers = [];
                for (let user of users) {
                    if (user.username != null && !(/^\s*$/.test(user.username))) {
                        filteredUsers.push(user);
                    }
                }
                res.status(200).send({ success: true, data: filteredUsers });

            }
            else {
                res.status(201).send({ success: false, message: "Failed" });
            }
        });
    }

    public async rankListSipsSent(req: Request, res: Response) {
        User.find({}).sort({ 'gameStats.busfahrerStats.lifeTimeSentSips': -1 }).limit(10).exec(function (err, users) {
            if (users) {
                let filteredUsers = [];
                for (let user of users) {
                    if (user.username != null && !(/^\s*$/.test(user.username))) {
                        filteredUsers.push(user);
                    }
                }
                res.status(200).send({ success: true, data: filteredUsers });
            }
            else {
                res.status(201).send({ success: false, message: "Failed" });
            }
        });
    }


    public async rankListGamesPlayed(req: Request, res: Response) {
        User.find({}).sort({ 'gameStats.busfahrerStats.gamesPlayed': -1 }).limit(10).exec(function (err, users) {
            if (users) {
                let filteredUsers = [];
                for (let user of users) {
                    if (user.username != null && !(/^\s*$/.test(user.username))) {
                        filteredUsers.push(user);
                    }
                }
                res.status(200).send({ success: true, data: filteredUsers });
            }
            else {
                res.status(201).send({ success: false, message: "Failed" });
            }
        });
    }

    public async rankListSipsRatio(req: Request, res: Response) {
        let minGamesPlayed = 5;
        User.find({ 'gameStats.busfahrerStats.gamesPlayed': { $gt: minGamesPlayed } }).sort({ 'gameStats.busfahrerStats.sipsRatio': -1 }).limit(10).exec(function (err, users) {
            if (users) {
                let filteredUsers = [];
                for (let user of users) {
                    if (user.username != null && !(/^\s*$/.test(user.username))) {
                        filteredUsers.push(user);
                    }
                }
                res.status(200).send({ success: true, data: filteredUsers });
            }
            else {
                res.status(201).send({ success: false, message: "Failed" });
            }
        });
    }

    //Skat

}