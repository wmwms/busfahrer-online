import { Document, Schema, Model, model, Error } from "mongoose";
import { PlayerGameStats } from './gameStats';

export interface IUser extends Document {
    username: string;
    currentSocketId: string;
    gameStats: PlayerGameStats;
    created_date: Date;
}

export const userSchema: Schema = new Schema({
    username: {
        type: String
    },
    currentSocketId: {
        type: String
    },
    gameStats: {
        type: Object,
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

export const User: Model<IUser> = model<IUser>("User", userSchema);