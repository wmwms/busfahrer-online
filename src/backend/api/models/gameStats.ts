export class PlayerGameStats {
    busfahrerStats: BusfahrerStats = new BusfahrerStats();
    skatStats: SkatStats = new SkatStats();
}

export class BusfahrerStats {
    gamesPlayed: number = 0;
    lifeTimeReceivedSips: number = 0;
    lifeTimeSentSips: number = 0;
    sipsRatio: number = 0;
}
export class SkatStats {

}