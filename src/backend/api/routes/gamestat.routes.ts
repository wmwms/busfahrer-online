import { Router } from "express";
import { GameStatController } from '../controllers/gamestat.controller';
import { AuthController } from '../controllers/auth.controller';

export class GameStatRoutes {

    router: Router;
    public authController: AuthController = new AuthController();
    public gamestatsController: GameStatController = new GameStatController();

    constructor() {
        this.router = Router();
        this.routes();
    }
    routes() {
        this.router.get("/busfahrer/rank-sipsReceived", this.gamestatsController.rankListSipsReceived);
        this.router.get("/busfahrer/rank-sipsSent", this.gamestatsController.rankListSipsSent);
        this.router.get("/busfahrer/rank-gamesPlayed", this.gamestatsController.rankListGamesPlayed);
        this.router.get("/busfahrer/rank-sipsRatio", this.gamestatsController.rankListSipsRatio);

    }
}