import { createServer, Server } from 'http';
import * as express from 'express';
import * as compression from 'compression';
import * as cors from 'cors';
import * as path from 'path';
import { PORT } from './util/secret';
import { SocketServer } from './socket/SocketServer';
import { UserRoutes } from './api/routes/user.routes';
import { GameStatRoutes } from './api/routes/gamestat.routes';
import { mongooseDB } from './db';


class AppServer {
    private app: express.Application = express();
    private server: Server;
    private router: any = express.Router();
    private socketServer: SocketServer;
    private db: mongooseDB;

    constructor() {
        this.app = express();
        this.config();
        //API
    }

    private config() {
        this.app.set("port", PORT || 32000);
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(compression());
        this.app.use(cors());
        // Default route
        this.routes();

        this.server = createServer(this.app);
        this.socketServer = new SocketServer(this.server);
        this.db = new mongooseDB();

    }

    private routes() {
        this.app.get('*.*', express.static(path.join(__dirname, '../frontend/')));
        this.app.use("/api/user", new UserRoutes().router);
        this.app.use("/api/gamestats", new GameStatRoutes().router);

        // Default route
        this.app.all('*', function (req, res) {
            res.status(200).sendFile('/', { root: path.join(__dirname, '../frontend/') });
        });
        return;
    }

    public listen(): void {
        this.server.listen(this.app.get("port"), () => {
            console.log('Running server on port %s', this.app.get("port"));
        });
    }
}

let appserver = new AppServer();
appserver.listen();