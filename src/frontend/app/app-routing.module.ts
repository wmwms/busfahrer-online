import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { IndexComponent } from './main/index/index.component';
import { SkatComponent } from './main/gameRoom/skat/skat.component';
import { BusDriverComponent } from './main/gameRoom/bus-driver/bus-driver.component';
import { BusMobilComponent } from './main/gameRoom/bus-mobil/bus-mobil.component';


function GetRoutes() {
    if (window.innerWidth >= 720) {
        return [
            {
                path: '',
                component: MainLayoutComponent,
                children: [
                    { path: '', component: IndexComponent },
                    { path: 'room/:id', component: BusDriverComponent },
                    { path: 'skat/:id', component: SkatComponent },
                    { path: 'bus-driver/:id', component: BusDriverComponent },
                ]
            }
        ];
    }
    else {
        return [
            {
                path: '',
                component: MainLayoutComponent,
                children: [
                    { path: '', component: IndexComponent },
                    { path: 'room/:id', component: BusMobilComponent },
                    { path: 'skat/:id', component: SkatComponent },
                    { path: 'bus-driver/:id', component: BusMobilComponent },
                ]
            }
        ];
    }

}
const routes: Routes = GetRoutes();


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
