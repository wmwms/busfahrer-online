import { Injectable } from '@angular/core';
import { EnglishLanguage } from '../language-files/english';
import { GermanLanguage } from '../language-files/german'
@Injectable({
    providedIn: 'root'
})
export class LanguageService {
    languagesArr: Array<Language> = [new EnglishLanguage(), new GermanLanguage()]
    currentLanguage: Language;
    constructor() {
        let lanName = localStorage.getItem('language');
        if (lanName) {
            this.currentLanguage = this.getLanguageByName(lanName);
        }
        else {
            this.currentLanguage = this.languagesArr[0];
        }
    }

    getLanguageByName(name: string) {
        for (let language of this.languagesArr) {
            if (language.languageName == name) {
                return language;
            }
        }
        return;
    }

    updateLanguage(language: Language) {
        this.currentLanguage = language;
        localStorage.setItem('language', language.languageName);
    }
}

export interface Language {
    languageName: string;
    //header
    settings: string;
    busdriver: string;
    // index
    createRoomDescribiton: string;
    createRoom: string;
    joinRoomDescribiton: string;
    joinRoom: string;
    tutorial: string;
    topStats: string;
    mostDrunk: string;
    mostShared: string;
    bestKDA: string;
    sipsDrunk: string;
    sipsShared: string;
    sipsRatio: string;
    //tutorial Dialog
    tutorialHeader: string;
    beginningPhase: string;
    beginningPhaseDescr: string;
    pyramidPhaseDescr: string;
    busdriverDescr: string;
    moreTips: string;
    moreTipsDescr: string;

    //room
    waitingForPlayers: string;
    ready: string;
    ownCards: string;
    guessColor: string;
    red: string;
    black: string;
    highLow: string;
    higher: string;
    lower: string;
    inOut: string;
    within: string;
    outside: string;
    containsOrNot: string;
    contains: string;
    notContains: string;

    pyramid: string;

    //roomNew
    prepPyramid: string;
    nextCard: string;

    //chat messages?
    playerJoined: string;
    playerReady: string;
    gameStarting: string;
    playerTurn: string;

    //sips dialog
    availableSips: string;
    whoShouldDrink: string;
    youReceive: string;
    drink: string;
    wrong: string;
    drinkSips: string;
    correct: string;

    //username dialog
    setUsername: string;

    //settings
    username: string;
    update: string;
    language: string;
}
