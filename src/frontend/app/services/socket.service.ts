import { Injectable } from '@angular/core';
import { Socket, io } from 'socket.io-client';

@Injectable({
    providedIn: 'root'
})
export class SocketService {
    socket: Socket;
    username;
    constructor() {
        this.username = localStorage.getItem("username");
        if (!this.username) {
            this.username = "Anon"
        }
    }

    setUsername(username) {
        this.username = username;
        localStorage.setItem("username", username);
    }

    socketConnect() {
        let baseUrl;
        if (window.origin.includes("localhost")) {
            baseUrl = "http://localhost:4000";
        }
        else {
            baseUrl = window.origin;
        }
        console.log("Connecting Socket at " + baseUrl);

        this.socket = io(baseUrl, {
            reconnection: true,             // whether to reconnect automatically
            reconnectionAttempts: Infinity, // number of reconnection attempts before giving up
            reconnectionDelay: 1000,        // how long to initially wait before attempting a new reconnection
            reconnectionDelayMax: 5000,     // maximum amount of time to wait between reconnection attempts. Each attempt increases the reconnection delay by 2x along with a randomization factor
            randomizationFactor: 0.5
        });

    }

    socketDisconnect() {
        this.socket.disconnect();
    }
}
