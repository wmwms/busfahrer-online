import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})

export class ApiService {
    baseUrl = window.origin;
    headers = new HttpHeaders();
    user;
    token;
    constructor(private http: HttpClient) {
        if (window.origin.includes("localhost")) {
            this.baseUrl = "http://localhost:4000";
        }
        else {
            this.baseUrl = window.origin;
        }
        console.log("Expecting Api at " + this.baseUrl + "/api");
        this.headers.set('Content-Type', 'application/json');
        this.token = localStorage.getItem('jwtToken');
        if (this.token) {
            this.getProfile();
        }
        else {
            this.createToken();
        }
    }

    async createToken() {
        this.http.get(`${this.baseUrl}/api/user/new`).subscribe((res: any) => {
            if (res.success) {
                this.token = res.token;
                localStorage.setItem('jwtToken', res.token);
                this.user = res.user;
            }

        });
    }

    async getProfile() {
        if (this.token) {
            const header = { Authorization: this.token };
            this.http.get(`${this.baseUrl}/api/user/get`, { headers: header }).subscribe((res: any) => {
                if (res.success) {
                    this.token = res.token;
                    localStorage.setItem('jwtToken', res.token);
                    this.user = res.user;
                }
                else {
                    this.createToken();
                }
            });
        }
    }

    async updateUser(data: any) {
        if (this.token) {
            let url = `${this.baseUrl}/api/user/update`;
            const header = { Authorization: this.token };
            this.http.put(url, data, { headers: header }).subscribe((res: any) => {
                if (res.success) {
                    this.getProfile();
                }
            });
        }
    }

    getTopSipsSent(): Observable<any> {
        let url = `${this.baseUrl}/api/gamestats/busfahrer/rank-sipsSent`;
        return this.http.get(url, { headers: this.headers });
    }

    getTopSipsReceived(): Observable<any> {
        let url = `${this.baseUrl}/api/gamestats/busfahrer/rank-sipsReceived`;
        return this.http.get(url, { headers: this.headers });
    }

    getTopSipsRatio(): Observable<any> {
        let url = `${this.baseUrl}/api/gamestats/busfahrer/rank-sipsRatio`;
        return this.http.get(url, { headers: this.headers });
    }

    getTopGamesPlayed(): Observable<any> {
        let url = `${this.baseUrl}/api/gamestats/busfahrer/rank-gamesPlayed`;
        return this.http.get(url, { headers: this.headers });
    }
}