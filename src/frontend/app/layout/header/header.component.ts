import { Component, NgZone, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Router } from '@angular/router';
import { SocketService } from '../../services/socket.service';
import { UserinfoComponent } from '../../main/userinfo/userinfo.component';
import { ApiService } from '../../services/api.service';
import { LanguageService } from '../../services/language.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    username;
   // langArray: Array<string> = ['DE', 'EN']; 
   langArray;

    constructor(private userinfo: MatBottomSheet, private ngZone: NgZone, private router: Router, private apiService: ApiService, public languageService: LanguageService) { }

    ngOnInit(): void {
        let interval = setInterval(() => {
            if (this.apiService && this.apiService.user) {
                this.username = this.apiService.user.username;
                console.log("Api ready");
                clearInterval(interval);
            }
            else {
                console.log("Api not ready");
            }
        }, 100);

        this.langArray = this.languageService.languagesArr;
    }

    updateLanguage(lang){
        this.languageService.updateLanguage(lang);
    }

    openUserInfo() {
        this.userinfo.open(UserinfoComponent);
    }

}
