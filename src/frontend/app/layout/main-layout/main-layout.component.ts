import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

    constructor(private titleService: Title) {
        titleService.setTitle("Test");
        let str = "🚌";
        setInterval(() => {
            if (str.length < 20) {
                str = "." + str
            }
            else {
                str = "🚌";
            }
            titleService.setTitle(str);
        }, 300);
    }

    ngOnInit(): void {
    }

}
