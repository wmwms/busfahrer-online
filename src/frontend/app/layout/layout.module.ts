import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';


@NgModule({
    declarations: [MainLayoutComponent, HeaderComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([]),
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
    ],
    exports: [
        MainLayoutComponent,
    ],
})
export class LayoutModule { }
