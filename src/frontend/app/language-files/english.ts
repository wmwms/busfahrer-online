import { Language } from '../services/language.service';
export class EnglishLanguage implements Language {
    languageName: string = "English";
    //header
    settings: string = "Settings";
    busdriver: string = "Busdriver";
    // index
    createRoomDescribiton: string;
    createRoom: string = "Create room";
    joinRoomDescribiton: string;
    joinRoom: string = "Join room";
    tutorial: string = "Tutorial";
    topStats: string = "Top Stats (Sort by clicking)";
    mostDrunk: string = "Most Sips Received";
    mostShared: string = "Most Sips Given";
    bestKDA: string = "Best ratio";
    sipsDrunk: string = "Sips received";
    sipsShared: string = "Sips shared";
    sipsRatio: string = "Ratio";
    //tutorial Dialog
    tutorialHeader: string = "";
    beginningPhase: string;
    beginningPhaseDescr: string;
    pyramidPhaseDescr: string;
    busdriverDescr: string;
    moreTips: string;
    moreTipsDescr: string;

    //room
    waitingForPlayers: string = "Waiting for players...";
    ready: string = "Ready";
    ownCards: string = "Own Cards";
    guessColor: string = "Guess color";
    red: string = "Red";
    black: string = "Black";
    highLow: string = "Higher or lower";
    higher: string = "Higher";
    lower: string = "Lower";
    inOut: string = "Within or not";
    within: string = "Within";
    outside: string = "Outside";
    containsOrNot: string = "Contains suit or not";
    contains: string = "Contains";
    notContains: string = "Not contains";

    pyramid: string = "Pyramid";

    //roomNew
    prepPyramid: string = "Done. Preparing pyramid...";
    nextCard: string = "Next!";

    //chat messages?
    playerJoined: string;
    playerReady: string;
    gameStarting: string;
    playerTurn: string;

    //sips dialog
    availableSips: string = "You can give away ";
    whoShouldDrink: string = "Who should drink how many?";
    youReceive: string = "You receive ";
    sips: string = "sips"
    drink: string = "Drink!";
    wrong: string = "Wrong!";
    drinkSips: string = "Drink ";
    correct: string = "Correct!";

    //username dialog
    setUsername: string = "Please set a valid username first!";

    //settings
    username: string = "Username";
    update: string = "Update";
    language: string = "Language";
}
