import { Language } from '../services/language.service';
export class GermanLanguage implements Language {
    languageName: string = "Deutsch";
    //header
    settings: string = "Einstellungen";
    busdriver: string = "Busfahrer";
    // index
    createRoomDescribiton: string;
    createRoom: string = "Raum erstellen";
    joinRoomDescribiton: string;
    joinRoom: string = "Raum beitreten";
    tutorial: string = "Tutorial";
    topStats: string = "Top Statistiken (Klicken zum Sortieren)";
    mostDrunk: string = "Meiste Getrunken";
    mostShared: string = "Meiste Verteilt";
    bestKDA: string = "Bestes Verhältnis";
    sipsDrunk: string = "Schlücke Getrunken";
    sipsShared: string = "Schlücke Verteilt";
    sipsRatio: string = "Verhätlnis";
    //tutorial Dialog
    tutorialHeader: string = "";
    beginningPhase: string;
    beginningPhaseDescr: string;
    pyramidPhaseDescr: string;
    busdriverDescr: string;
    moreTips: string;
    moreTipsDescr: string;

    //room
    waitingForPlayers: string = "Warte auf Spieler...";
    ready: string = "Bereit";
    ownCards: string = "Eigene Karten";
    guessColor: string = "Farbe raten";
    red: string = "rot";
    black: string = "schwarz";
    highLow: string = "Höher oder niedriger";
    higher: string = "Höher";
    lower: string = "Niedriger";
    inOut: string = "Innerhalb oder außerhalb";
    within: string = "Innerhalb";
    outside: string = "Außerhalb";
    containsOrNot: string = "Symbol dabei oder nicht dabei";
    contains: string = "Dabei";
    notContains: string = "Nicht dabei";

    pyramid: string = "Pyramide";

    //roomNew
    prepPyramid: string = "Fertig. Pyramide wird vorbereitet...";
    nextCard: string = "Nächste!";

    //chat messages?
    playerJoined: string;
    playerReady: string;
    gameStarting: string;
    playerTurn: string;

    //sips dialog
    availableSips: string = "Verteilbare Schlücke: ";
    whoShouldDrink: string = "Wer soll wie viele trinken?";
    youReceive: string = "Du erhältst ";
    sips: string = "Schlücke"
    drink: string = "Trink!";
    wrong: string = "Falsch!";
    drinkSips: string = "Trink ";
    correct: string = "Richtig!";

    //username dialog
    setUsername: string = "Bitte zunächst einen validen Username setzen!";

    //settings
    username: string = "Benutzername";
    update: string = "aktualisieren";
    language: string = "Sprache";
}
