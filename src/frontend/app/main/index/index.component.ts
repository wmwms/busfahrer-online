import { Component, NgZone, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SocketService } from '../../services/socket.service';
import { UserinfoComponent } from '../userinfo/userinfo.component';
import { TutDialogComponent } from './tut-dialog/tut-dialog.component';
import { ApiService } from '../../services/api.service';
import { UsernameDialogComponent } from '../username-dialog/username-dialog.component';
import { LanguageService } from '../../services/language.service';

export interface Player {
    name: string;
    position: number;
    sipsSent: number;
    sipsDrunk: number;
    sipsRatio: string;
    gamesPlayed: number;
}

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
    id;
    rankList;

    displayedColumns: string[] = ['position', 'name', 'sipsDrunk', 'sipsSent', 'sipsRatio', 'gamesPlayed'];
    activeColumnIndex: number;
    dataSource: Player[];

    constructor(private apiService: ApiService, private userinfo: MatBottomSheet, private ngZone: NgZone, private router: Router, public dialog: MatDialog, public languageService: LanguageService,) {

    }



    ngOnInit(): void {
        if (this.apiService) {
            this.rankListSipsDrunk();
        }
    }

    rankListSipsDrunk() {
        if (this.activeColumnIndex != 2) {
            this.apiService.getTopSipsReceived().subscribe((res) => {
                this.rankList = res.data;
                let erray: Player[] = [];
                this.rankList.forEach((value, index) => {
                    erray.push({ position: index + 1, name: value.username, sipsDrunk: value.gameStats.busfahrerStats.lifeTimeReceivedSips, sipsSent: value.gameStats.busfahrerStats.lifeTimeSentSips, sipsRatio: value.gameStats.busfahrerStats.sipsRatio, gamesPlayed: value.gameStats.busfahrerStats.gamesPlayed });

                });
                this.dataSource = erray;
            }, (error) => {
                console.log(error);
            });
        }

        this.activeColumnIndex = 2;
    }

    rankListSipsSent() {
        if (this.activeColumnIndex != 3) {
            this.rankList = this.apiService.getTopSipsSent().subscribe((res) => {
                this.rankList = res.data;
                let erray: Player[] = [];
                this.rankList.forEach((value, index) => {
                    erray.push({ position: index + 1, name: value.username, sipsDrunk: value.gameStats.busfahrerStats.lifeTimeReceivedSips, sipsSent: value.gameStats.busfahrerStats.lifeTimeSentSips, sipsRatio: value.gameStats.busfahrerStats.sipsRatio, gamesPlayed: value.gameStats.busfahrerStats.gamesPlayed });
                });
                this.dataSource = erray;
            }, (error) => {
                console.log(error);
            });
        }
        this.activeColumnIndex = 3;

    }

    rankListSipsRatio() {
        if (this.activeColumnIndex != 4) {
            this.rankList = this.apiService.getTopSipsRatio().subscribe((res) => {
                this.rankList = res.data;
                let erray: Player[] = [];
                this.rankList.forEach((value, index) => {
                    erray.push({ position: index + 1, name: value.username, sipsDrunk: value.gameStats.busfahrerStats.lifeTimeReceivedSips, sipsSent: value.gameStats.busfahrerStats.lifeTimeSentSips, sipsRatio: value.gameStats.busfahrerStats.sipsRatio, gamesPlayed: value.gameStats.busfahrerStats.gamesPlayed });

                });
                this.dataSource = erray;
            }, (error) => {
                console.log(error);
            });
        }
        this.activeColumnIndex = 4;
    }

    rankListGamesPlayed() {
        if (this.activeColumnIndex != 5) {
            this.rankList = this.apiService.getTopGamesPlayed().subscribe((res) => {
                this.rankList = res.data;
                let erray: Player[] = [];
                this.rankList.forEach((value, index) => {
                    erray.push({ position: index + 1, name: value.username, sipsDrunk: value.gameStats.busfahrerStats.lifeTimeReceivedSips, sipsSent: value.gameStats.busfahrerStats.lifeTimeSentSips, sipsRatio: value.gameStats.busfahrerStats.sipsRatio, gamesPlayed: value.gameStats.busfahrerStats.gamesPlayed });

                });
                this.dataSource = erray;
            }, (error) => {
                console.log(error);
            });
        }
        this.activeColumnIndex = 5;
    }




    openUserInfo() {
        this.userinfo.open(UserinfoComponent);
    }


    createRoom() {
        if (this.apiService.user.username == undefined || this.apiService.user.username.length < 2) {
            this.openSetUsername('createRoom');
        }
        else {
            this.ngZone.run(() => this.router.navigateByUrl('/room/' + Math.random().toString(36).substr(2, 9)));
        }
    }

    join(link) {
        window.location = link;
    }

    openSetUsername(fncName: string): void {
        const dialogRef = this.dialog.open(UsernameDialogComponent, {
            width: '20%',
            height: '12%',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            // wait 250ms for backend answer (api Service set username)
            setTimeout(() => {
                if (fncName == 'createRoom') {
                    this.createRoom();
                }
            }, 250);

        });
    }

    openTutorial(): void {
        const dialogRef = this.dialog.open(TutDialogComponent, {
            width: '60%',
            height: '70%',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
        });
    }

}


