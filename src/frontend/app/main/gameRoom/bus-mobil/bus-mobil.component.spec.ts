import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusMobilComponent } from './bus-mobil.component';

describe('BusMobilComponent', () => {
  let component: BusMobilComponent;
  let fixture: ComponentFixture<BusMobilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusMobilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusMobilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
