import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SocketService } from '../../../services/socket.service';
import { BusDriverSocket } from '../bus-driver/bus-driver.socket';
import { BusDriverPlayer } from '../../../model/player';
import { MatDialog } from '@angular/material/dialog';
import { SipsDialogComponent } from '../bus-driver/sips-dialog/sips-dialog.component';
import { ApiService } from '../../../services/api.service';
import { UsernameDialogComponent } from '../../username-dialog/username-dialog.component';
import { LanguageService } from '../../../services/language.service';
import { TutDialogComponent } from '../../index/tut-dialog/tut-dialog.component';


@Component({
    selector: 'app-bus-mobil',
    templateUrl: './bus-mobil.component.html',
    styleUrls: ['./bus-mobil.component.scss']
})
export class BusMobilComponent implements OnInit {
    roomId: string;
    roomSocket: BusDriverSocket;
    phase = -1;
    players: Array<BusDriverPlayer> = [];
    playerMap: Map<BusDriverPlayer, string[]> = new Map();
    self: BusDriverPlayer;
    ownCards: Array<string> = [];
    //Phase 1
    rounds = ['redOrBlack', 'highOrLow', 'betweenOrNot', 'containsSuitOrNot'];
    roundDesc;// constructor with language files
    options = [['red', 'black'], ['high', 'low'], ['true', 'false'], ['true', 'false']];
    optionsDesc; // constructor with language files
    // Phase 2
    pyramidArray: Array<string> = [];
    // Phase 3
    busArray: Array<string> = [];
    msgs: string[] = [];

    constructor(private route: ActivatedRoute, private socketService: SocketService, private apiService: ApiService, public dialog: MatDialog, public languageService: LanguageService) {
        this.route.params.subscribe((params: Params) => {
            this.roomId = params['id'];
        });
        this.roundDesc = [languageService.currentLanguage.guessColor, languageService.currentLanguage.highLow, languageService.currentLanguage.inOut, languageService.currentLanguage.containsOrNot];
        this.optionsDesc = [[languageService.currentLanguage.red, languageService.currentLanguage.black], [languageService.currentLanguage.higher, languageService.currentLanguage.lower], [languageService.currentLanguage.within, languageService.currentLanguage.outside], [languageService.currentLanguage.contains, languageService.currentLanguage.notContains]];

    }

    ngOnInit(): void {
        if (!this.socketService.socket || !this.socketService.socket.connected) {
            this.socketService.socketConnect();
        }
        this.roomSocket = new BusDriverSocket(this, this.socketService.socket);

        let interval = setInterval(() => {
            if (this.apiService && this.apiService.user && this.socketService.socket.connected) {
                this.apiService.updateUser({ currentSocketId: this.socketService.socket.id });
                clearInterval(interval);
                let int2 = setInterval(() => {
                    if (this.socketService.socket.connected) {
                        this.join();
                        clearInterval(int2);
                    }
                }, 100);
            }
            else {
                console.log("Api not ready");
            }
        }, 100);
    }

    openDialog(correct, sipsAvail): void {
        const dialogRef = this.dialog.open(SipsDialogComponent, {
            width: '350px',
            data: { room: this, correct: correct, sipsAvail: sipsAvail, mobile: true }
        });

        dialogRef.afterClosed().subscribe(result => {
            //console.log('The dialog was closed');
        });
    }


    openDialogDrink(amount, fromPlayer): void {
        const dialogRef = this.dialog.open(SipsDialogComponent, {
            data: { room: this, amount: amount, fromPlayer: fromPlayer }
        });

        dialogRef.afterClosed().subscribe(result => {
            //console.log('The dialog was closed');
        });
    }

    openSetUsername(fncName: string): void {
        const dialogRef = this.dialog.open(UsernameDialogComponent, {
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            setTimeout(() => {
                // wait 250ms for backend answer
                if (fncName == 'ready') {
                    this.ready();
                }
            }, 250);

        });
    }

    openTutorial(): void {
        const dialogRef = this.dialog.open(TutDialogComponent, {
            width: '60%',
            height: '70%',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
        });
    }

    join() {
        this.roomSocket.socket.emit('join', this.roomId, 'busdriver');
    }

    guess(round, value) {
        this.roomSocket.socket.emit('guess', this.roomId, round, value);
    }

    sendDrinks(amount: number, toSocketID) {
        this.roomSocket.socket.emit('sendDrinks', this.roomId, amount, toSocketID);
    }

    putCardOnPyramid(card: string) {
        this.roomSocket.socket.emit('putCardOnPyramid', this.roomId, card);
    }

    guessBus(cardPosition) {
        this.roomSocket.socket.emit('guessBus', this.roomId, cardPosition);
    }

    ready() {
        if (this.apiService.user.username == undefined || this.apiService.user.username.length < 2) {
            this.openSetUsername('ready');
        }
        else {
            this.updateLanguageArr();
            this.roomSocket.socket.emit("ready", this.roomId);
        }

    }
    nextCard() {
        this.roomSocket.socket.emit("nextCard", this.roomId)
    }

    updateLanguageArr() {
        this.roundDesc = [this.languageService.currentLanguage.guessColor, this.languageService.currentLanguage.highLow, this.languageService.currentLanguage.inOut, this.languageService.currentLanguage.containsOrNot];
        this.optionsDesc = [[this.languageService.currentLanguage.red, this.languageService.currentLanguage.black], [this.languageService.currentLanguage.higher, this.languageService.currentLanguage.lower], [this.languageService.currentLanguage.within, this.languageService.currentLanguage.outside], [this.languageService.currentLanguage.contains, this.languageService.currentLanguage.notContains]];
    }
}



