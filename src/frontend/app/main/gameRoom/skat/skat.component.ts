import { Component, OnInit } from '@angular/core';
import { SkatSocket } from './skat.socket';
import { ActivatedRoute, Params } from '@angular/router';
import { SocketService } from 'src/frontend/app/services/socket.service';
import { ApiService } from 'src/frontend/app/services/api.service';


@Component({
    selector: 'app-skat',
    templateUrl: './skat.component.html',
    styleUrls: ['./skat.component.scss']
})
export class SkatComponent implements OnInit {
    roomId;
    roomSocket;
    msgs = [];
    ownCards: Array<string>;
    playerMap;
    players;
    self;
    selectedVal = 12; //eichel preselect
    phase = 0;
    currentSkatTravel;
    tableCards: string[] = [];
    skatCards: string[] = [];
    constructor(private route: ActivatedRoute, private socketService: SocketService, private apiService: ApiService) {
        this.route.params.subscribe((params: Params) => {
            this.roomId = params['id'];
        });
    }

    ngOnInit(): void {
        if (!this.socketService.socket || !this.socketService.socket.connected) {
            this.socketService.socketConnect();
        }
        this.roomSocket = new SkatSocket(this, this.socketService.socket);

        let interval = setInterval(() => {
            if (this.apiService && this.apiService.user && this.socketService.socket.connected) {
                this.apiService.updateUser({ currentSocketId: this.socketService.socket.id });
                clearInterval(interval);
                let int2 = setInterval(() => {
                    if (this.socketService.socket.connected) {
                        this.join();
                        clearInterval(int2);
                    }
                }, 100);
            }
            else {
                console.log("Api not ready");
            }
        }, 100);
    }


    join() {
        this.roomSocket.socket.emit('join', this.roomId, 'skat');
    }


    ready() {
        this.roomSocket.socket.emit("ready", this.roomId);
    }

    setPlayingTypeValue($event) {
        console.log(this.selectedVal);
        this.roomSocket.socket.emit("setPlayingTypeValue", this.roomId, this.selectedVal);
    }

    toggleOuvert() {
        this.roomSocket.socket.emit("toggleOuvert", this.roomId);

    }

    toggleHand() {
        this.roomSocket.socket.emit("toggleHand", this.roomId);
    }

    skatTravelAnswer(bool: boolean) {
        this.roomSocket.socket.emit('skatTravelAnswer', this.roomId, bool);
        this.currentSkatTravel = null;
    }

    sortCards() {
        this.ownCards.sort((b, a) => {
            if (this.getCardValue(a) == 'U' && this.getCardValue(b) == 'U') {
                if (this.getCardSuit(a) == 'E') {
                    return 1;
                }
                else if (this.getCardSuit(a) == 'G' && this.getCardSuit(b) != 'E') {
                    return 1;
                }
                else if (this.getCardSuit(a) == 'H' && this.getCardSuit(b) != 'E' && this.getCardSuit(b) != 'G') {
                    return 1;
                }
                else {
                    return -1
                }
            }
            else if (this.getCardValue(a) == 'U' && this.getCardValue(b) != 'U') {
                return 1;
            }
            else if (this.getCardValue(a) != 'U' && this.getCardValue(b) == 'U') {
                return -1;
            }
            else if (this.getCardSuit(a) == this.getCardSuit(b)) {
                if (this.isCardHigher(a, b)) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
            else if (this.getCardSuit(a) == 'E') {
                return 1;
            }
            else if (this.getCardSuit(a) == 'G' && this.getCardSuit(b) != 'E') {
                return 1;
            }
            else if (this.getCardSuit(a) == 'H' && this.getCardSuit(b) != 'E' && this.getCardSuit(b) != 'G') {
                return 1;
            }
            else {
                return -1
            }

        });
    }

    getCardSuit(card): string {
        // if value is 10
        if (card.length == 3) {
            return card[2];
        }
        return card[1];
    }

    getCardValue(card: string): string {
        // if value is 10
        if (card.length == 3) {
            return card[0] + card[1];
        }
        return card[0];
    }

    isCardHigher(card1: string, card2: string) {
        let card1Val = this.getCardValue(card1);
        let card2Val = this.getCardValue(card2);
        // check if its a number
        let card1Num = Number(card1Val);
        let card2Num = Number(card2Val);
        // false if same, or 1. card is number and 2. card is not (J,Q,K)
        if (card1Val == card2Val || (card1Num && !card2Num && card1Num != 10)) {
            return false;
        }
        // true if first card not number (J,Q,K) and second is, or both are numbers but first is higher
        else if ((!card1Num && card2Num) || (card1Num && card2Num && card1Num > card2Num)) {
            return true;
        }
        // true if first A (second also A already checked above)
        else if (card1Val == 'A') {
            return true
        }
        // true if first K and second Q or K (second also K already checked above)
        else if (card1Val == '10' && (card2Val == 'O' || card2Val == 'K')) {
            return true
        }

        // true if first Q and second J  (second also J already checked above)
        else if (card1Val == 'K' && card2Val == 'O') {
            return true
        }
        // first J akready checked in 1. (J == J) and 2. (J > number)   
        return false;
    }

    playCard(card) {
        if (this.phase == 2 && this.ownCards.length > 10 && this.ownCards.includes(card)) {
            this.ownCards.splice(this.ownCards.indexOf(card), 1);
            this.skatCards.push(card);
        }
        else if (this.phase == 4) {
            this.roomSocket.socket.emit("playCard", this.roomId, card);
        }

    }

    putSkatAway() {
        if (this.phase == 2 && this.ownCards.length == 10 && this.skatCards.length == 2) {
            this.roomSocket.socket.emit('putSkatAway', this.roomId, this.skatCards[0], this.skatCards[1]);
        }
    }

    setGameType() {
        this.roomSocket.socket.emit('setGameType', this.roomId);
    }


}
