import { SkatComponent } from './skat.component';
import { Socket } from 'socket.io-client';

export class SkatSocket {
    socket: Socket;

    constructor(private room: SkatComponent, private socketRef: Socket) {
        this.socket = socketRef;
        this.socketReact();
    }


    socketReact() {
        this.socket.on('connection', () => {
        });

        this.socket.on('rejoin', () => {
            this.room.join();
        });

        this.socket.on('msg', (msg) => {
            this.room.msgs.push(msg);
            let chatDiv = document.getElementById('chat');
            setTimeout(() => chatDiv.scrollTop = chatDiv.scrollHeight, 100); // let view initialize first
        });

        this.socket.on('getupdate', () => {
            this.socket.emit('getupdate', this.room.roomId);
        });


        this.socket.on('selfUpdate', (ownCards: Array<string>, self: any) => {
            this.room.ownCards = ownCards;
            this.room.sortCards();
            this.room.self = self;
            console.log(ownCards);
            console.log(self);
        });




        this.socket.on('playerMap', (playerMapStr: string) => {
            console.log(playerMapStr);
            this.room.playerMap = new Map(JSON.parse(playerMapStr));
            this.room.players = [];
            for (let player of this.room.playerMap.keys()) {
                this.room.players.push(player);
            };
        });

        this.socket.on('phase', (phase: number) => {
            this.room.phase = phase;
            if (phase == 1) {
                this.room.skatCards = [];
            }
        })

        this.socket.on('skatTravelValue', (value: string) => {
            this.room.currentSkatTravel = value;
        });

        this.socket.on('tableCards', (cards: string[]) => {
            this.room.tableCards = cards;
        });

        this.socket.on('skatCards', (cards: string[]) => {
            this.room.skatCards = cards;
        });
    }

    socketJoin() {
    }


}