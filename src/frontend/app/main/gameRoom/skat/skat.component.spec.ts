import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkatComponent } from './skat.component';

describe('SkatComponent', () => {
  let component: SkatComponent;
  let fixture: ComponentFixture<SkatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
