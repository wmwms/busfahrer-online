import { BusDriverComponent } from './bus-driver.component';
import { Socket } from 'socket.io-client';
import { BusDriverPlayer } from '../../../model/player';

export class BusDriverSocket {
    socket;
    constructor(private room: any, private socketRef: Socket) {
        this.socket = socketRef;
        this.socketReact();
    }


    socketReact() {
        this.socket.on('connection', () => {
        });

        this.socket.on('rejoin', () => {
            this.room.join();
        });

        this.socket.on('msg', (msg) => {
            this.room.msgs.push(msg);
            let chatDiv = document.getElementById('chat');
            setTimeout(() => chatDiv.scrollTop = chatDiv.scrollHeight, 100); // let view initialize first
        });

        this.socket.on('getupdate', () => {
            this.socket.emit('getupdate', this.room.roomId);
        });

        this.socket.on('selfUpdate', (ownCards: Array<string>, self: BusDriverPlayer) => {
            this.room.ownCards = ownCards;
            this.room.self = self;
        })

        this.socket.on('pyramidArray', (pyramidArray: Array<string>) => {
            this.room.pyramidArray = pyramidArray;
        })

        this.socket.on('busArray', (busArray: Array<string>) => {
            this.room.busArray = busArray;
        })

        this.socket.on('guess', (correct: boolean, sipsAvail: number) => {
            this.room.openDialog(correct, sipsAvail);
        })

        this.socket.on('putCardOnPyramid', (valid: boolean, sipsAvail: number) => {
            if (valid == true) {
                this.room.openDialog(valid, sipsAvail);
            }
        })

        this.socket.on('receiveDrinks', (amount: number, fromPlayer: string) => {
            this.room.openDialogDrink(amount, fromPlayer);
        })

        this.socket.on('playerMap', (playerMapStr: string) => {
            console.log(playerMapStr);
            this.room.playerMap = new Map(JSON.parse(playerMapStr));
            this.room.players = [];
            for (let player of this.room.playerMap.keys()) {
                this.room.players.push(player);
            };
        });

        this.socket.on('phase', (phase: number) => {
            this.room.phase = phase;
        })
    }

    socketJoin() {
    }


}