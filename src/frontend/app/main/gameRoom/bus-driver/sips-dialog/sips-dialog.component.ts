import { Component, Inject, OnInit } from '@angular/core';
import { BusDriverPlayer } from '../../../../model/player';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BusDriverComponent } from '../bus-driver.component';
import { LanguageService } from '../../../../services/language.service';

export interface DialogData {
    room: BusDriverComponent;
    correct: boolean;
    players: Array<BusDriverPlayer>;
    amount: number;
    fromPlayer: string;
    sipsAvail: number;
    mobile: boolean;
}

@Component({
    selector: 'app-sips-dialog',
    templateUrl: './sips-dialog.component.html',
    styleUrls: ['./sips-dialog.component.scss']
})
export class SipsDialogComponent implements OnInit {
    room: BusDriverComponent;
    correct: boolean;
    players: Array<BusDriverPlayer>;
    amount: number;
    fromPlayer: string;
    sipsAvail: number;
    mobile: boolean;

    constructor(
        public dialogRef: MatDialogRef<SipsDialogComponent>,
        public languageService: LanguageService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {
        this.room = data.room;
        this.correct = data.correct;
        this.players = data.room.players;
        this.fromPlayer = data.fromPlayer;
        this.amount = data.amount;
        this.sipsAvail = data.sipsAvail;
        this.mobile = data.mobile;
    }

    onOkClick(): void {
        let inputs = document.getElementsByTagName('input');
        for (let i = 0; i < this.players.length; i++) {
            let value = Number(inputs[i].value);
            if (value && value != 0) {
                this.room.sendDrinks(value, this.players[i].socketID);
            }
        }
        this.dialogRef.close();
    }

    ngOnInit(): void {
    }

}
