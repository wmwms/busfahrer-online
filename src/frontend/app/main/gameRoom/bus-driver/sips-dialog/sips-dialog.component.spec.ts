import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SipsDialogComponent } from './sips-dialog.component';

describe('SipsDialogComponent', () => {
  let component: SipsDialogComponent;
  let fixture: ComponentFixture<SipsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SipsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SipsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
