import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { ApiService } from '../../services/api.service';

@Component({
    selector: 'app-userinfo',
    templateUrl: './userinfo.component.html',
    styleUrls: ['./userinfo.component.scss']
})
export class UserinfoComponent implements OnInit {
    username;
    constructor(private _bottomSheetRef: MatBottomSheetRef<UserinfoComponent>, private apiService: ApiService) { }
    ngOnInit(): void {
        let interval = setInterval(() => {
            if (this.apiService && this.apiService.user) {
                this.username = this.apiService.user.username;
                console.log("Api ready");
                clearInterval(interval);
            }
            else {
                console.log("Api not ready");
            }
        }, 100);
    }

    updateUsername(newUsername: string) {
        this.apiService.updateUser({ username: newUsername });
        this._bottomSheetRef.dismiss();
    }

    openLink(event: MouseEvent): void {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
    }

}
