import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-username-dialog',
  templateUrl: './username-dialog.component.html',
  styleUrls: ['./username-dialog.component.scss']
})
export class UsernameDialogComponent implements OnInit {
  username;

  constructor(private apiService: ApiService, public dialogRef: MatDialogRef<UsernameDialogComponent>,public languageService: LanguageService,) { }

  ngOnInit(): void {
      let interval = setInterval(() => {
        if (this.apiService && this.apiService.user) {
            this.username = this.apiService.user.username;
            console.log("Api ready");
            clearInterval(interval);
        }
        else {
            console.log("Api not ready");
        }
    }, 100);
  }
  
  updateUsername(newUsername: string) {
    this.apiService.updateUser({ username: newUsername });
    this.dialogRef.close();
  }
}
