import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './main/index/index.component';
import { UserinfoComponent } from './main/userinfo/userinfo.component';

import { RouterModule, Routes } from '@angular/router';

import { LayoutModule } from './layout/layout.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SipsDialogComponent } from './main/gameRoom/bus-driver/sips-dialog/sips-dialog.component';
import { MatListModule } from '@angular/material/list';
import { TutDialogComponent } from './main/index/tut-dialog/tut-dialog.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { UsernameDialogComponent } from './main/username-dialog/username-dialog.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSelectModule } from '@angular/material/select';


import { BusDriverComponent } from './main/gameRoom/bus-driver/bus-driver.component';
import { SkatComponent } from './main/gameRoom/skat/skat.component';
import { BusMobilComponent } from './main/gameRoom/bus-mobil/bus-mobil.component';


@NgModule({
    declarations: [
        AppComponent,
        IndexComponent,
        UserinfoComponent,
        SipsDialogComponent,
        TutDialogComponent,
        UsernameDialogComponent,
        BusDriverComponent,
        SkatComponent,
        BusMobilComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        LayoutModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatFormFieldModule,
        MatBottomSheetModule,
        MatDialogModule,
        MatCheckboxModule,
        MatListModule,
        MatTabsModule,
        MatDividerModule,
        MatTableModule,
        MatMenuModule,
        MatBadgeModule,
        MatSelectModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
